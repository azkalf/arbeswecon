<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function images()
    {
        return $this->hasMany('App\Models\ProjectImage', 'project_id');
    }

    public function principal()
    {
        return $this->belongsTo('App\Models\Employee', 'principal_id');
    }

    public function ded()
    {
        return $this->belongsTo('App\Models\Employee', 'ded_id');
    }

    public function projectCategories()
    {
        return $this->hasMany('App\Models\ProjectCategory', 'project_id');
    }
}
