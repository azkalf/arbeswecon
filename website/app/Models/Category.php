<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function projectCategories()
    {
        return $this->hasMany('App\Models\ProjectCategory', 'category_id');
    }
}
