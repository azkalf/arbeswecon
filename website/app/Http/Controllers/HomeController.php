<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function inbox()
    {
        return view('mailbox.index');
    }

    public function inboxGetData()
    {
        $messages = Message::latest()->get();
        foreach ($messages as $message) {
            $message->from = $message->read ? $message->name . '<br>' . $message->email : '<strong>'.$message->name.'<br>'.$message->email. '</strong>';
            $message->subject = $message->read ? '<a href="'.url('inbox/read/'.$message->id).'">'.$message->subject.'</a>' : '<a href="' . url('inbox/read/' . $message->id) . '"><strong>' . $message->subject . '</strong></a><br>';
            $message->send_date = date('Y-m-d H:i:s', strtotime($message->created_at));
        }
        return response()->json(['data' => $messages]);
    }

    public function inboxDelete(Request $request)
    {
        $success = false;
        $mail = Message::find($request->id);
        if ($mail->delete()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function inboxRead($id)
    {
        $message = Message::find($id);
        if (!$message) {
            abort(404);
        }
        $message->read = true;
        $message->save();
        return view('mailbox.read', compact('message'));
    }

    public function badgeUpdate(Request $request)
    {
        if ($request->param == 'inbox') {
            return Message::where('read', false)->count();
        } else {
            return 0;
        }
    }
}
