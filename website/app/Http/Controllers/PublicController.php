<?php

namespace App\Http\Controllers;

use App\Models\AboutImage;
use App\Models\Category;
use App\Models\Client;
use App\Models\Employee;
use App\Models\Message;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\SocialMedia;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();
        return view('public.index', compact('sliders'));
    }

    public function projects()
    {
        $projects = Project::orderBy('order')->get();
        $categories = Category::all();
        return view('public.projects', compact('projects', 'categories'));
    }

    public function projectCategory($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if (!$category) {
            abort('404');
        }
        $projectIds = ProjectCategory::where('category_id', $category->id)->pluck('project_id')->toArray();
        $projects = Project::whereIn('id', $projectIds)->get();
        $categories = Category::all();
        return view('public.projects', compact('projects', 'categories', 'category'));
    }

    public function projectDetail($id)
    {
        $project = Project::find($id);
        $prevProject = Project::where('id', '<', $id)->orderBy('id', 'desc')->first();
        $nextProject = Project::where('id', '>', $id)->orderBy('id', 'asc')->first();
        $otherProjects = Project::where('id', '!=', $id)->latest()->take(10)->get();
        return view('public.project_detail', compact('project', 'otherProjects', 'prevProject', 'nextProject'));
    }

    public function contact()
    {
        $setting = Setting::first();
        $socmeds = SocialMedia::all();
        return view('public.contact', compact('setting', 'socmeds'));
    }

    public function services()
    {
        $services = Service::all();
        return view('public.services', compact('services'));
    }

    public function about()
    {
        $setting = Setting::first();
        $images = AboutImage::all();
        return view('public.about', compact('setting', 'images'));
    }

    public function employeeDetail($idcard)
    {
        $employee = Employee::where('idcard', 'like', "%$idcard%")->first();
        if (!$employee) {
            abort('404');
        }
        $employee->photo_url = $employee->photo ? asset('storage/employee/'.$employee->photo) : asset('assets/images/user.jpg');
        return view('public.employee_detail', compact('employee'));
    }

    public function sendmail(Request $request)
    {
        $message = new Message;
        $message->name = $request->name;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;
        $message->read = false;
        if ($message->save()) {
            return 'success';
        } else {
            return 'failed';
        }
    }
}
