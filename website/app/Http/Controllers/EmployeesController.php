<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('employees.index');
    }

    public function getData()
    {
        $employees = Employee::latest()->get();
        foreach ($employees as $employee) {
            $employee->photo_image = $employee->photo ? '<img src="'.asset('storage/employee/'.$employee->photo).'" style="height: 50px;" />' : '<img src="' . asset('assets/images/user.jpg') . '" style="height: 50px;" />';
        }
        return response()->json(['data' => $employees]);
    }

    public function save(Request $request)
    {
        $success = false;
        $employee = new Employee;
        if ($request->employee_id) {
            $employee = Employee::find($request->employee_id);
        }
        try {
            DB::beginTransaction();
            $employee->idcard = $request->idcard;
            $employee->fullname = $request->fullname;
            $employee->nickname = $request->nickname;
            $employee->position = $request->position;
            $employee->description = $request->description;
            if ($employee->save()) {
                $success = true;
                if ($request->hasFile('photo')) {
                    $filename = "employee_photo_" . $employee->id . "." . $request->file('photo')->extension();
                    $request->file('photo')->storeAs('employee', $filename, 'public');
                    $employee->photo = $filename;
                    if (!$employee->save()) {
                        $success = false;
                    }
                }
            }
        } catch (\Throwable $th) {
            dd($th);
        }
        if ($success) {
            DB::commit();
        } else {
            DB::rollback();
        }
        return response()->json(['success' => $success]);
    }

    public function getEmployee($id)
    {
        $employee = Employee::find($id);
        $employee->photo_image = $employee->photo ? asset('storage/employee/'.$employee->photo) : asset('assets/images/user.jpg');
        return response()->json($employee);
    }

    public function delete(Request $request)
    {
        $success = false;
        $employee = Employee::find($request->id);
        if ($employee->delete()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }
}
