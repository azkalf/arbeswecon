<?php

namespace App\Http\Controllers;

use App\Models\AboutImage;
use App\Models\Category;
use App\Models\Client;
use App\Models\Project;
use App\Models\ProjectCategory;
use App\Models\ProjectImage;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $setting = Setting::first();
        return view('settings.index', compact('setting'));
    }

    public function getSetting()
    {
        $setting = Setting::first();
        if (!$setting) {
            return [];
        }
        $setting->about_images = '';
        $aboutImages = AboutImage::all();
        $key = 0;
        foreach ($aboutImages as $image) {
            $setting->about_images .= '
            <div class="col-md-4" id="aimagediv' . $key . '">
                <div class="card mb-2 bg-gradient-dark">
                    <img class="img-fluid card-img-top" src="' . asset('storage/abouts/thumb_' . $image->image . '?' . date('His')) . '"">
                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <span class="btr">
                            <a href="javascript:void(0);" onclick="removeAboutImages(' . $key . ')"><i class="fas fa-trash text-danger" title="Remove Image"></i></a>
                        </span>
                    </div>
                </div>
                <input type="hidden" name="old_about_images[' . $image->id . ']" value="' . $image->image . '">
            </div>';
            $key++;
        }
        $setting->about_image_key = $key;
        return response()->json($setting);
    }

    public function getSlider($id)
    {
        $slider = Slider::find($id);
        $slider->image_src = asset('storage/sliders/'.$slider->image) . '?' . date('His');
        return response()->json($slider);
    }

    public function getSliders()
    {
        $sliders = Slider::all();
        $slider_list = '';
        foreach ($sliders as $slider) {
            $slider_list .= '
            <div class="col-md-4 slider-item-db">
                <div class="card mb-2 bg-gradient-dark">
                    <img class="card-img-top" src="'.asset('storage/sliders/thumb_'.$slider->image).'?'.date('His').'" alt="'.$slider->title.'">
                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <span class="btr">
                            <a href="javascript:void(0);" onclick="editSlider('.$slider->id. ')"><i class="fas fa-edit text-warning" title="Edit Slider"></i></a>
                            <a href="javascript:void(0);" onclick="removeSlider(' . $slider->id . ')"><i class="fas fa-trash text-danger" title="Remove Slider"></i></a>
                        </span>
                        <h5 class="card-title text-primary text-white">'.$slider->title.'</h5>
                        <p class="card-text text-white pb-2 pt-1">'.$slider->subtitle.'</p>
                    </div>
                </div>
            </div>';
        }
        return $slider_list;
    }

    public function saveSlider(Request $request)
    {
        $success = false;
        $slider = new Slider;
        if ($request->slider_id) {
            $slider = Slider::find($request->slider_id);
        }
        try {
            DB::beginTransaction();
            $slider->title = $request->title;
            $slider->subtitle = $request->subtitle;
            if ($slider->save()) {
                $success = true;
                if ($request->hasFile('image')) {
                    Storage::disk('public')->delete('sliders/' . $slider->image);
                    Storage::disk('public')->delete('sliders/file_' . $slider->image);
                    Storage::disk('public')->delete('sliders/thumb_' . $slider->image);
                    $filename = "slider_image_" . $slider->id . "." . $request->file('image')->extension();
                    $request->file('image')->storeAs('sliders', $filename, 'public');
                    $slider->image = $filename;
                    $image = Storage::disk('public')->get("sliders/$filename");
                    $img = Image::make($image);
                    $img->resize(1366, 768, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(public_path("storage/sliders/file_$filename"));
                    $thumb = Image::make($image);
                    $thumb->fit(400, 225, function ($constraint) {
                        $constraint->upsize();
                    })->save(public_path("storage/sliders/thumb_$filename"));
                    if (!$slider->save()) {
                        $success = false;
                    }
                }
            }
        } catch (\Throwable $th) {
            dd($th);
        }
        if ($success) {
            DB::commit();
        } else {
            DB::rollback();
        }
        return response()->json(['success' => $success]);
    }

    public function removeSlider(Request $request)
    {
        $success = false;
        $slider = Slider::find($request->id);
        if ($slider->delete()) {
            $success = true;
            Storage::disk('public')->delete('sliders/' . $slider->image);
            Storage::disk('public')->delete('sliders/file_' . $slider->image);
            Storage::disk('public')->delete('sliders/thumb_' . $slider->image);
        }
        return response()->json(['success' => $success]);
    }

    public function getAbout()
    {
        $setting = Setting::first();
        $aboutImages = AboutImage::all();
        $image_list = '<div class="hero-slider">';
        foreach ($aboutImages as $image) {
            $image_list .= '<div class="slider-item th-fullpage hero-area" style="background-image: url('.asset('storage/abouts/file_'.$image->image).');"></div>';
        }
        $image_list .= '</div>';
        $about = '
        <div class="col-md-6">'.$image_list.'</div>
        <div class="col-md-6">'
            .$setting->about_description.
        '</div>';
        return $about;
    }

    public function saveAbout(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->about_title = $request->about_title;
        $setting->about_subtitle = $request->about_subtitle;
        $setting->about_description = $request->about_description;
        $aboutImages = AboutImage::all();
        foreach ($aboutImages as $old_image) {
            if (!isset($request->old_about_images[$old_image->id])) {
                $old_image->delete();
                Storage::disk('public')->delete('abouts/' . $old_image->image);
                Storage::disk('public')->delete('abouts/file_' . $old_image->image);
                Storage::disk('public')->delete('abouts/thumb_' . $old_image->image);
            }
        }
        if ($request->hasFile('about_images')) {
            foreach ($request->file('about_images') as $input_image) {
                $aboutImage = new AboutImage;
                if ($aboutImage->save()) {
                    $filename = "about_image_$aboutImage->id." . $input_image->extension();
                    $input_image->storeAs('abouts', $filename, 'public');
                    $aboutImage->image = $filename;
                    $image = Storage::disk('public')->get("abouts/$filename");
                    $img = Image::make($image);
                    $img->resize(1366, 768, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(public_path("storage/abouts/file_$filename"));
                    $thumb = Image::make($image);
                    $thumb->fit(400, 225, function ($constraint) {
                        $constraint->upsize();
                    })->save(public_path("storage/abouts/thumb_$filename"));
                    if (!$aboutImage->save()) {
                        $success = false;
                    }
                } else {
                    $success = false;
                }
            }
        }
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getProjectTitle()
    {
        $setting = Setting::first();
        if ($setting && $setting->project_title) {
            $project_title = '
                <h3>' . $setting->project_title . '</h3>
                <p>' . $setting->project_subtitle . '</p>';
        } else {
            $project_title = '
                <h3>Title</h3>
                <p>Subtitle</p>';
        }
        return $project_title;
    }

    public function saveProjectTitle(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->project_title = $request->project_title;
        $setting->project_subtitle = $request->project_subtitle;
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getCategories()
    {
        $categories = Category::all();
        return response()->json(['data' => $categories]);
    }

    public function getCategoryOptions()
    {
        $categories = Category::all();
        $list = '';
        foreach ($categories as $category) {
            $list .= '<option value="' . $category->id . '">' . $category->name . '</option>';
        }
        return $list;
    }

    public function saveCategory(Request $request)
    {
        $success = false;
        $category = new Category;
        if ($request->category_id) {
            $category = Category::find($request->category_id);
        }
        $category->name = $request->name;
        $category->slug = $request->slug;
        $category->description = $request->description;
        if ($category->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function removeCategory(Request $request)
    {
        $success = false;
        try {
            DB::beginTransaction();
            ProjectCategory::where('category_id', $request->id)->delete();
            $category = Category::find($request->id);
            if ($category->delete()) {
                $success = true;
            }
        } catch (\Throwable $th) {
            dd($th);
        }
        if ($success) {
            DB::commit();
        } else {
            DB::rollback();
        }
        return response()->json(['success' => $success]);
    }

    public function getCategory($id)
    {
        $category = Category::find($id);
        return response()->json($category);
    }

    public function getProjects()
    {
        $projects = Project::orderBy('order')->get();
        foreach ($projects as $project) {
            $images = '';
            foreach ($project->images as $image) {
                $images .= '<img src="'.asset('storage/projects/thumb_'.$image->image). '" class="mr-1 mb-1" height="50px" />';
            }
            $principal = $project->principal ? $project->principal->nickname.' ('.$project->principal->fullname.')' : '';
            $ded = $project->ded ? $project->ded->nickname.' ('.$project->ded->fullname.')' : '';
            if ($project->projectCategories) {
                $category_ids = $project->projectCategories->pluck('category_id')->toArray();
                $categories = Category::whereIn('id', $category_ids)->pluck('name')->toArray();
                $project->categories = implode(', ', $categories);
            } else {
                $project->categories = '';
            }
            $project->desc = '
            <div class="row">
                <div class="col-md-4">
                    <img src="'.asset('storage/projects/thumb_'.$project->image).'" class="img-fluid"><hr>'.$images.'
                </div>
                <div class="col-md-8">
                    <table class="table table-borderless table-projects-list">
                        <tr>
                            <th width="100" class="text-left">Category</th>
                            <td width="1">:</td>
                            <td>'.$project->categories.'</td>
                        </tr>
                        <tr>
                            <th width="100" class="text-left">Name</th>
                            <td width="1">:</td>
                            <td>'.$project->name.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Year</th>
                            <td>:</td>
                            <td>'.$project->year.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Location</th>
                            <td>:</td>
                            <td>'.$project->location.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Principal</th>
                            <td>:</td>
                            <td>'.$principal.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">DED</th>
                            <td>:</td>
                            <td>'.$ded.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Renderer</th>
                            <td>:</td>
                            <td>'.$project->renderer.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Sorted</th>
                            <td>:</td>
                            <td>'.$project->order.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Team</th>
                            <td>:</td>
                            <td>'.$project->team.'</td>
                        </tr>
                        <tr>
                            <th class="text-left">Description</th>
                            <td>:</td>
                            <td>'.$project->description.'</td>
                        </tr>
                    </table>
                </div>
            </div>';
        }
        return response()->json(['data' => $projects]);
    }

    public function saveProject(Request $request)
    {
        $success = false;
        $project = new Project;
        if ($request->project_id) {
            $project = Project::find($request->project_id);
        }
        try {
            DB::beginTransaction();
            $project->name = $request->name;
            $project->year = $request->year;
            $project->location = $request->location;
            $project->renderer = $request->renderer;
            $project->principal_id = $request->principal_id;
            $project->ded_id = $request->ded_id;
            $project->description = $request->description;
            $project->order = $request->order;
            if ($project->save()) {
                $success = true;
                ProjectCategory::where('project_id', $project->id)->delete();
                if ($request->category_ids) {
                    foreach ($request->category_ids as $category_id) {
                        $projectCategory = ProjectCategory::where('project_id', $project->id)->where('category_id', $category_id)->first();
                        if (!$projectCategory) {
                            $projectCategory = new ProjectCategory;
                        }
                        $projectCategory->project_id = $project->id;
                        $projectCategory->category_id = $category_id;
                        if (!$projectCategory->save()) {
                            $success = false;
                        }
                    }
                }
                if ($request->hasFile('image')) {
                    Storage::disk('public')->delete('projects/' . $project->image);
                    Storage::disk('public')->delete('projects/file_' . $project->image);
                    Storage::disk('public')->delete('projects/thumb_' . $project->image);
                    $filename = "project_" . $project->id . "." . $request->file('image')->extension();
                    $request->file('image')->storeAs('projects', $filename, 'public');
                    $project->image = $filename;
                    $image = Storage::disk('public')->get("projects/$filename");
                    $img = Image::make($image);
                    $img->resize(1366, 768, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save(public_path("storage/projects/file_$filename"));
                    $thumb = Image::make($image);
                    $thumb->fit(400, 225, function ($constraint) {
                        $constraint->upsize();
                    })->save(public_path("storage/projects/thumb_$filename"));
                    if (!$project->save()) {
                        $success = false;
                    }
                }
                foreach ($project->images as $old_image) {
                    if (!isset($request->old_project_images[$old_image->id])) {
                        $old_image->delete();
                        Storage::disk('public')->delete('projects/' . $old_image->image);
                        Storage::disk('public')->delete('projects/file_' . $old_image->image);
                        Storage::disk('public')->delete('projects/thumb_' . $old_image->image);
                    }
                }
                if ($request->hasFile('project_images')) {
                    foreach ($request->file('project_images') as $input_image) {
                        $projectImage = new ProjectImage;
                        $projectImage->project_id = $project->id;
                        if ($projectImage->save()) {
                            $filename = "project_".$project->id."_image_$projectImage->id." . $input_image->extension();
                            $input_image->storeAs('projects', $filename, 'public');
                            $projectImage->image = $filename;
                            $image = Storage::disk('public')->get("projects/$filename");
                            $img = Image::make($image);
                            $img->resize(1366, 768, function ($constraint) {
                                $constraint->aspectRatio();
                                $constraint->upsize();
                            })->save(public_path("storage/projects/file_$filename"));
                            $thumb = Image::make($image);
                            $thumb->fit(400, 225, function ($constraint) {
                                $constraint->upsize();
                            })->save(public_path("storage/projects/thumb_$filename"));
                            if (!$projectImage->save()) {
                                $success = false;
                            }
                        } else {
                            $success = false;
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            dd($th);
        }
        if ($success) {
            DB::commit();
        } else {
            DB::rollback();
        }
        return response()->json(['success' => $success]);
    }

    public function removeProject(Request $request)
    {
        $success = false;
        $project = Project::find($request->id);
        foreach ($project->images as $image) {
            Storage::disk('public')->delete('projects/' . $image->image);
            $image->delete();
        }
        if ($project->delete()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getProject($id)
    {
        $project = Project::find($id);
        $project->category_id = $project->projectCategories->pluck('category_id')->toArray();
        $project->categoryOptions = $this->getCategoryOptions();
        $project->image_src = asset('storage/projects/thumb_' . $project->image.'?'.date('His'));
        $project->images_list = '';
        $key = 0;
        foreach ($project->images as $image) {
            $project->images_list .= '
            <div class="col-md-4" id="imagediv'.$key.'">
                <div class="card mb-2 bg-gradient-dark">
                    <img class="img-fluid card-img-top" src="'.asset('storage/projects/thumb_'.$image->image . '?' . date('His')).'"">
                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <span class="btr">
                            <a href="javascript:void(0);" onclick="removeProjectImages('.$key. ')"><i class="fas fa-trash text-danger" title="Remove Image"></i></a>
                        </span>
                    </div>
                </div>
                <input type="hidden" name="old_project_images['.$image->id.']" value="'.$image->image.'">
            </div>';
            $key++;
        }
        $project->image_key = $key;
        return response()->json($project);
    }

    public function getClientTitle()
    {
        $setting = Setting::first();
        if ($setting && $setting->client_title) {
            $client_title = '
                <h3>' . $setting->client_title . '</h3>
                <p>' . $setting->client_subtitle . '</p>';
        } else {
            $client_title = '
                <h3>Title</h3>
                <p>Subtitle</p>';
        }
        return $client_title;
    }

    public function saveClientTitle(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->client_title = $request->client_title;
        $setting->client_subtitle = $request->client_subtitle;
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function saveClient(Request $request)
    {
        $success = false;
        $client = new Client;
        if ($request->client_id) {
            $client = Client::find($request->client_id);
        }
        try {
            DB::beginTransaction();
            $client->name = $request->name;
            $client->description = $request->description;
            if ($client->save()) {
                $success = true;
                if ($request->hasFile('client_logo')) {
                    $filename = "client_logo_" . $client->id . "." . $request->file('client_logo')->extension();
                    $request->file('client_logo')->storeAs('clients', $filename, 'public');
                    $client->logo = $filename;
                    $image = Storage::disk('public')->get("clients/$filename");
                    $img = Image::make($image);
                    $img->resize(400, 225, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })
                    ->resizeCanvas(400, 225, 'center', false, 'ffffff')
                    ->save(public_path("storage/clients/file_$filename"));
                    if (!$client->save()) {
                        $success = false;
                    }
                }
            }
        } catch (\Throwable $th) {
            dd($th);
        }
        if ($success) {
            DB::commit();
        } else {
            DB::rollback();
        }
        return response()->json(['success' => $success]);
    }

    public function getClient($id)
    {
        $client = Client::find($id);
        $client->logo_src = asset('storage/clients/' . $client->logo . '?' . date('His'));
        return response()->json($client);
    }

    public function getClients()
    {
        $clients = Client::all();
        $client_list = '';
        foreach ($clients as $client) {
            $client_list .= '
            <div class="col-md-4 client-item-db">
                <span class="btr">
                    <a href="javascript:void(0);" onclick="editClient(' . $client->id . ')"><i class="fas fa-edit text-warning" title="Edit Client"></i></a>
                    <a href="javascript:void(0);" onclick="removeClient(' . $client->id . ')"><i class="fas fa-trash text-danger" title="Remove Client"></i></a>
                </span>
                <span class="mailbox-attachment-icon has-img"><img src="'.asset('storage/clients/file_'.$client->logo . '?' . date('His')).'" alt="'.$client->name.'" class="img-fluid"></span>
                <div class="mailbox-attachment-info">
                    <a href="javascript:void(0);" class="mailbox-attachment-name">'.$client->name.'</a>
                    <p>'.$client->description.'</p>
                </div>
            </div>';
        }
        return $client_list;
    }

    public function removeClient(Request $request)
    {
        $success = false;
        $client = Client::find($request->id);
        if ($client->delete()) {
            Storage::disk('public')->delete('clients/' . $client->logo);
            Storage::disk('public')->delete('clients/file_' . $client->logo);
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getServiceTitle()
    {
        $setting = Setting::first();
        if ($setting && $setting->service_title) {
            $service_title = '
                <h3>' . $setting->service_title . '</h3>
                <p>' . $setting->service_subtitle . '</p>';
        } else {
            $service_title = '
                <h3>Title</h3>
                <p>Subtitle</p>';
        }
        return $service_title;
    }

    public function saveServiceTitle(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->service_title = $request->service_title;
        $setting->service_subtitle = $request->service_subtitle;
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getServices()
    {
        $services = Service::all();
        $service_list = '';
        foreach ($services as $service) {
            $service_list .= '
            <div class="col-md-4 service-item-db">
                <span class="btr">
                    <a href="javascript:void(0);" onclick="editService(' . $service->id . ')"><i class="fas fa-edit text-warning" title="Edit Service"></i></a>
                    <a href="javascript:void(0);" onclick="removeService(' . $service->id . ')"><i class="fas fa-trash text-danger" title="Remove Service"></i></a>
                </span>
                <span class="mailbox-attachment-icon has-img"><img src="' . asset('storage/services/file_' . $service->icon . '?' . date('His')) . '" alt="' . $service->name . '" class="img-fluid"></span>
                <div class="mailbox-attachment-info">
                    <a href="javascript:void(0);" class="mailbox-attachment-name">' . $service->name . '</a>
                </div>
            </div>';
        }
        return $service_list;
    }

    public function saveService(Request $request)
    {
        $success = false;
        $service = new Service;
        if ($request->service_id) {
            $service = Service::find($request->service_id);
        }
        $service->name = $request->name;
        $service->description = $request->description;
        if ($service->save()) {
            if ($request->hasFile('service_icon')) {
                $filename = "service_icon_" . $service->id . "." . $request->file('service_icon')->extension();
                $request->file('service_icon')->storeAs('services', $filename, 'public');
                $service->icon = $filename;
                $image = Storage::disk('public')->get("services/$filename");
                $img = Image::make($image);
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save(public_path("storage/services/file_$filename"));
                if (!$service->save()) {
                    $success = false;
                }
            }
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getService($id)
    {
        $service = Service::find($id);
        $service->icon_src = asset('storage/services/' . $service->icon . '?' . date('His'));
        return response()->json($service);
    }

    public function removeService(Request $request)
    {
        $success = false;
        $service = Service::find($request->id);
        if ($service->delete()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getContact()
    {
        $setting = Setting::first();
        if ($setting && $setting->contact_title) {
            $contact = '
            <div class="col-md-6">
                '.$setting->contact_map.'
            </div>
            <div class="col-md-6">
                <strong>Email Receiver:</strong> '.$setting->contact_email.'<hr>
                <h3>' . $setting->contact_title . '</h3>
                <p>' . $setting->contact_subtitle . '</p><hr>'
                . $setting->contact_description .
                '</div>';
        } else {
            $contact = '
            <div class="col-md-6">
                <img src="' . asset('assets/images/no-image.png') . '" alt="" class="img-fluid" />
            </div>
            <div class="col-md-6">
                <h3>Title</h3>
                <p>Subtitle</p><hr>
                contact description
            </div>';
        }
        return $contact;
    }

    public function saveContact(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->contact_email = $request->contact_email;
        $setting->contact_title = $request->contact_title;
        $setting->contact_subtitle = $request->contact_subtitle;
        $setting->contact_map = $request->contact_map;
        $setting->contact_description = $request->contact_description;
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getSocmeds()
    {
        $socmeds = SocialMedia::all();
        foreach ($socmeds as $socmed) {
            $socmed->name = '<i class="'.$socmed->icon. '"></i>&nbsp;&nbsp;'.$socmed->name;
        }
        return response()->json(['data' => $socmeds]);
    }

    public function getSocmed($id)
    {
        $socmed = SocialMedia::find($id);
        return response()->json($socmed);
    }

    public function saveSocmed(Request $request)
    {
        $success = false;
        $socmed = SocialMedia::find($request->socmed_id);
        if (!$socmed) {
            $socmed = new SocialMedia;
        }
        $socmed->icon = $request->icon;
        $socmed->name = $request->name;
        $socmed->link = $request->link;
        if ($socmed->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function removeSocmed(Request $request)
    {
        $success = false;
        $socmed = SocialMedia::find($request->id);
        if ($socmed->delete()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }

    public function getFooterTitle()
    {
        $setting = Setting::first();
        if ($setting && $setting->footer_title) {
            $footer_title = '
                <h3>' . $setting->footer_title . '</h3>
                <p>' . $setting->footer_subtitle . '</p>';
        } else {
            $footer_title = '
                <h3>Title</h3>
                <p>Subtitle</p>';
        }
        return $footer_title;
    }

    public function saveFooterTitle(Request $request)
    {
        $success = false;
        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting;
        }
        $setting->footer_title = $request->footer_title;
        $setting->footer_subtitle = $request->footer_subtitle;
        if ($setting->save()) {
            $success = true;
        }
        return response()->json(['success' => $success]);
    }
}
