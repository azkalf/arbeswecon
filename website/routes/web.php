<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@index')->name('index');
Route::get('/k/{idcard}', 'PublicController@employeeDetail');

Auth::routes(['register' => false]);

Route::get('/projects', 'PublicController@projects')->name('projects');
Route::get('/projects/{slug}', 'PublicController@projectCategory');
Route::get('/project/{id}', 'PublicController@projectDetail');
Route::get('/services', 'PublicController@services')->name('services');
Route::get('/contact', 'PublicController@contact')->name('contact');
Route::get('/about-us', 'PublicController@about')->name('about');

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/sendmail', 'PublicController@sendmail');

// Employees
Route::get('/employees', 'EmployeesController@index')->name('employees');
Route::get('/employees/getData', 'EmployeesController@getData');
Route::post('/employees/save', 'EmployeesController@save');
Route::get('/employees/getEmployee/{id}', 'EmployeesController@getEmployee');
Route::post('/employees/delete', 'EmployeesController@delete');

// Settings
Route::get('/settings', 'SettingsController@index')->name('settings');
Route::get('/settings/getSetting', 'SettingsController@getSetting');
// Slider
Route::get('/settings/getSliders', 'SettingsController@getSliders');
Route::get('/settings/getSlider/{id}', 'SettingsController@getSlider');
Route::post('/settings/saveSlider', 'SettingsController@saveSlider');
Route::post('/settings/removeSlider', 'SettingsController@removeSlider');
// About
Route::get('/settings/getAbout', 'SettingsController@getAbout');
Route::post('/settings/saveAbout', 'SettingsController@saveAbout');
// Project
Route::get('/settings/getProjectTitle', 'SettingsController@getProjectTitle');
Route::post('/settings/saveProjectTitle', 'SettingsController@saveProjectTitle');
Route::get('/settings/getCategories', 'SettingsController@getCategories');
Route::get('/settings/getCategory/{id}', 'SettingsController@getCategory');
Route::get('/settings/getCategoryOptions', 'SettingsController@getCategoryOptions');
Route::post('/settings/saveCategory', 'SettingsController@saveCategory');
Route::post('/settings/removeCategory', 'SettingsController@removeCategory');
Route::get('/settings/getProjects', 'SettingsController@getProjects');
Route::post('/settings/saveProject', 'SettingsController@saveProject');
Route::post('/settings/removeProject', 'SettingsController@removeProject');
Route::get('/settings/getProject/{id}', 'SettingsController@getProject');
// Client
Route::get('/settings/getClientTitle', 'SettingsController@getClientTitle');
Route::post('/settings/saveClientTitle', 'SettingsController@saveClientTitle');
Route::post('/settings/saveClient', 'SettingsController@saveClient');
Route::get('/settings/getClient/{id}', 'SettingsController@getClient');
Route::get('/settings/getClients', 'SettingsController@getClients');
Route::post('/settings/removeClient', 'SettingsController@removeClient');
// Service
Route::get('/settings/getServiceTitle', 'SettingsController@getServiceTitle');
Route::post('/settings/saveServiceTitle', 'SettingsController@saveServiceTitle');
Route::get('/settings/getService/{id}', 'SettingsController@getService');
Route::get('/settings/getServices', 'SettingsController@getServices');
Route::post('/settings/saveService', 'SettingsController@saveService');
Route::post('/settings/removeService', 'SettingsController@removeService');
// Contact
Route::get('/settings/getContact', 'SettingsController@getContact');
Route::post('/settings/saveContact', 'SettingsController@saveContact');
Route::get('/settings/getSocmeds', 'SettingsController@getSocmeds');
Route::get('/settings/getSocmed/{id}', 'SettingsController@getSocmed');
Route::post('/settings/saveSocmed', 'SettingsController@saveSocmed');
Route::post('/settings/removeSocmed', 'SettingsController@removeSocmed');
Route::post('/settings/saveFooterTitle', 'SettingsController@saveFooterTitle');
Route::get('/settings/getFooterTitle', 'SettingsController@getFooterTitle');

// Inbox
Route::get('/inbox', 'HomeController@inbox');
Route::get('/inbox/getData', 'HomeController@inboxGetData');
Route::post('/inbox/delete', 'HomeController@inboxDelete');
Route::get('/inbox/read/{id}', 'HomeController@inboxRead');

// Badge Update
Route::get('badge/update', 'HomeController@badgeUpdate');