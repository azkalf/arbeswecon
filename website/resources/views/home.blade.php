@extends('layouts.adminlte')
@section('title', 'Home')

@section('content')
<div class="card card-dark">
    <div class="card-header">
        <h5 class="card-title">Welcome</h5>
    </div>
    <div class="card-body">
        Welcome <b>{{auth()->user()->name}}</b>, have a nice day..
    </div>
</div>
@endsection
