@extends('layouts.adminlte')
@section('title', 'Page Settings')

@section('content')
{{-- Sliders --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Sliders</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addSlider();">Add Slider</a>
        <div class="row" id="sliders">
        </div>
    </div>
    <div class="overlay dark" id="sliders-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div>

{{-- Form Add Slider --}}
<div class="modal fade" id="modal-slider">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="slider-form-title">Add Slider</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveSlider')}}" id="form-slider" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="slider_id" id="slider_id">
                <div class="modal-body">
                    <input type="file" name="image" id="slider-image" style="display: none;">
                    <img src="{{asset('assets/images/no-image.png')}}" id="slider-image_thumbnail" onclick="addSliderImage();" height="100px" style="cursor: pointer;">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" class="form-control" id="slider-title" placeholder="Title">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- About Us --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">About Us</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editAbout();">Edit</a>
        <div class="row" id="about">
        </div>
    </div>
    <div class="overlay dark" id="about-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div>

{{-- Form About Us --}}
<div class="modal fade" id="modal-about">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="about-form-title">Edit About Us Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveAbout')}}" id="form-about" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="about_description">Description</label>
                        <textarea name="about_description" id="about_description" rows="3" class="form-control form-control-sm summernote" placeholder="Description"></textarea>
                    </div>
                    <h3>Images</h3>
                    <hr>
                    <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addAboutImages();">Add Image</a>
                    <input type="file" id="about-images" style="display: none;">
                    <div id="about-images-list" class="row"></div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Project Categories --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Project Categories</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addCategory();">Add Project Category</a>
        <table class="table table-hover table-bordered table-striped" id="table_categories">
            <thead>
                <th>Name</th>
                <th>Slug</th>
                <th>Description</th>
                <th width="40"></th>
            </thead>
        </table>
    </div>
</div>

{{-- Form Project Category --}}
<div class="modal fade" id="modal-category">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="category-form-title">Add Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveCategory')}}" id="form-category" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="category_id" id="category_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" id="category_name" placeholder="Name" required>
                        <input type="hidden" name="slug" class="form-control" id="category_slug" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="category_description" rows="3" class="form-control form-control-sm summernote" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Projects --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Projects</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        {{-- <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editProjectTitle();">Edit</a>
        <div id="project-title">
        </div>
        <hr> --}}
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addProject();">Add Project</a>
        <table class="table table-hover table-bordered table-striped" id="table_projects">
            <thead>
                <th>Project</th>
                <th width="40"></th>
            </thead>
        </table>
    </div>
</div>

{{-- Form Project --}}
<div class="modal fade" id="modal-project-title">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Project Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveProjectTitle')}}" id="form-project-title" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="project_title">Title</label>
                        <input type="text" name="project_title" class="form-control" id="project_title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="project_subtitle">Subtitle</label>
                        <textarea name="project_subtitle" id="project_subtitle" rows="3" class="form-control form-control-sm" placeholder="Subtitle"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Form Add Project --}}
<div class="modal fade" id="modal-project">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="employee-form-title">Add Project</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveProject')}}" id="form-project" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="project_id" id="project_id">
                <div class="modal-body">
                    <input type="file" name="image" id="project-image" style="display: none;">
                    <img src="{{asset('assets/images/no-image.png')}}" id="project-image_thumbnail" onclick="addProjectImage();" height="100px" style="cursor: pointer;">
                    <div class="form-group">
                        <label for="name">Project Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" id="project-name" placeholder="Project Name" required>
                    </div>
                    <div class="form-group">
                        <label for="category_id">Project Category</label>
                        {!! Form::select('category_ids[]', [], null, ['class' => 'form-control form-control-sm selectpicker', 'id' => 'project-category_id', 'data-live-search' => 'true', 'data-dropup-auto' => 'false', 'data-display' => 'static', 'multiple' => true]) !!}
                    </div>
                    <div class="form-group">
                        <label for="year">Year</label>
                        <input type="text" name="year" class="form-control" id="project-year" placeholder="Year">
                    </div>
                    <div class="form-group">
                        <label for="location">Location</label>
                        <input type="text" name="location" class="form-control" id="project-location" placeholder="Location">
                    </div>
                    <div class="form-group">
                        <label for="principal_id">Principal</label>
                        {!! Form::select('principal_id', employees(), null, ['class' => 'form-control form-control-sm selectpicker', 'id' => 'project-principal_id', 'data-live-search' => 'true', 'data-dropup-auto' => 'false', 'data-display' => 'static', 'placeholder' => 'Choose Option']) !!}
                    </div>
                    <div class="form-group">
                        <label for="ded_id">DED</label>
                        {!! Form::select('ded_id', employees(), null, ['class' => 'form-control form-control-sm selectpicker', 'id' => 'project-ded_id', 'data-live-search' => 'true', 'data-dropup-auto' => 'false', 'data-display' => 'static', 'placeholder' => 'Choose Option']) !!}
                    </div>
                    <div class="form-group">
                        <label for="renderer">3D Rendering</label>
                        <input type="text" name="renderer" class="form-control" id="project-renderer" placeholder="Renderer">
                    </div>
                    <div class="form-group">
                        <label for="team">Team</label>
                        <textarea name="team" id="project-team" rows="3" class="form-control form-control-sm summernote" placeholder="Team" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="order">Sorted</label>
                        <input type="text" name="order" class="form-control order" id="project-order" placeholder="Sorted">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="project-description" rows="3" class="form-control form-control-sm summernote" placeholder="Description" required></textarea>
                    </div>
                    <h3>Images</h3>
                    <hr>
                    <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addProjectImages();">Add Image</a>
                    <input type="file" id="project-images" style="display: none;">
                    <div id="project-images-list" class="row"></div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Clients --}}
{{-- <div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Clients</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editClientTitle();">Edit</a>
        <div id="client-title">
        </div>
        <hr>
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addClient();">Add Client</a>
        <div class="row" id="clients">
        </div>
    </div>
    <div class="overlay dark" id="client-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div> --}}

{{-- Form Client --}}
<div class="modal fade" id="modal-client-title">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Client Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveClientTitle')}}" id="form-client-title" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="client_title">Title</label>
                        <input type="text" name="client_title" class="form-control" id="client_title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="client_subtitle">Subtitle</label>
                        <textarea name="client_subtitle" id="client_subtitle" rows="3" class="form-control form-control-sm" placeholder="Subtitle"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Form Clients --}}
<div class="modal fade" id="modal-client">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="client-form-title">Add Client</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveClient')}}" id="form-client" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="client_id" id="client_id">
                <div class="modal-body">
                    <input type="file" name="client_logo" id="client_logo" style="display: none;">
                    <img src="{{asset('assets/images/no-image.png')}}" id="client_logo_thumbnail" onclick="addClientLogo();" height="100px" style="cursor: pointer;">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" id="client_name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label for="client_description">Description</label>
                        <textarea name="description" id="client_description" rows="3" class="form-control form-control-sm summernote" placeholder="Description" required></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Services --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Services</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        {{-- <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editServiceTitle();">Edit</a>
        <div id="service-title">
        </div>
        <hr> --}}
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addService();">Add Service</a>
        <div class="row" id="services">
        </div>
    </div>
    <div class="overlay dark" id="service-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div>

{{-- Form Service Title --}}
<div class="modal fade" id="modal-service-title">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit Service Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveServiceTitle')}}" id="form-service-title" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="service_title">Title</label>
                        <input type="text" name="service_title" class="form-control" id="service_title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="service_subtitle">Subtitle</label>
                        <textarea name="service_subtitle" id="service_subtitle" rows="3" class="form-control form-control-sm" placeholder="Subtitle"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Form Service --}}
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="service-form-title">Add Service</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveService')}}" id="form-service" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="service_id" id="service_id">
                <div class="modal-body">
                    <input type="file" name="service_icon" id="service_icon" style="display: none;">
                    <img src="{{asset('assets/images/no-image.png')}}" id="service_icon_thumbnail" onclick="addServiceIcon();" height="100px" style="cursor: pointer;">
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" id="service_name" placeholder="Name" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Contact Us --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Contact Us</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editContact();">Edit</a>
        <div class="row" id="contact">
        </div>
    </div>
    <div class="overlay dark" id="contact-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div>

{{-- Form Contact Us --}}
<div class="modal fade" id="modal-contact">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="contact-form-title">Edit Contact Us Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveContact')}}" id="form-contact" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- <div class="form-group">
                        <label for="contact_email">Email Receiver</label>
                        <input type="text" name="contact_email" class="form-control" id="contact_email" placeholder="Email Receiver">
                    </div>
                    <div class="form-group">
                        <label for="contact_title">Title</label>
                        <input type="text" name="contact_title" class="form-control" id="contact_title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="contact_subtitle">Subtitle</label>
                        <textarea name="contact_subtitle" id="contact_subtitle" rows="3" class="form-control form-control-sm" placeholder="Subtitle"></textarea>
                    </div> --}}
                    <div class="form-group">
                        <label for="contact_map">Map iFrame</label>
                        <input type="text" name="contact_map" class="form-control" id="contact_map" placeholder="Map iFrame">
                    </div>
                    <div class="form-group">
                        <label for="contact_description">Description</label>
                        <textarea name="contact_description" id="contact_description" rows="3" class="form-control form-control-sm summernote" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Social Media --}}
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Social Media</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-primary btn-flat mb-3" onclick="addSocMed();">Add Social Media</a>
        <table class="table table-hover table-bordered table-striped" id="table_socmed">
            <thead>
                <th>Name</th>
                <th>Link</th>
                <th width="40"></th>
            </thead>
        </table>
    </div>
</div>

{{-- Form Social Media --}}
<div class="modal fade" id="modal-socmed">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="socmed-form-title">Add Social Media</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveSocmed')}}" id="form-socmed" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="socmed_id" id="socmed_id">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="icon">Icon</label>
                        <select name="icon" id="socmed_icon" class="form-control form-control-sm selectpicker" data-live-search="true" data-dropup-auto="false" data-display="static" placeholder="Choose Option">
                            @foreach (thicons() as $key => $value)
                            <option value="{{$key}}" class="{{$key}}">&ensp;{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="socmed_name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" class="form-control" id="socmed_name" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label for="socmed_link">Link <span class="text-danger">*</span></label>
                        <input type="text" name="link" class="form-control" id="socmed_link" placeholder="Link" required>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Footer --}}
{{-- <div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title">Footer</h3>
        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="javascript:void(0);" class="btn btn-warning btn-flat mb-3" onclick="editFooterTitle();">Edit</a>
        <div id="footer-title">
        </div>
    </div>
    <div class="overlay dark" id="footer-loader" style="display:none;">
        <i class="fas fa-2x fa-sync-alt fa-spin"></i>
    </div>
</div> --}}

{{-- Form Footer --}}
<div class="modal fade" id="modal-footer-title">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="footer-form-title">Edit Footer Title</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('settings/saveFooterTitle')}}" id="form-footer-title" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="footer_title">Title</label>
                        <input type="text" name="footer_title" class="form-control" id="footer_title" placeholder="Title">
                    </div>
                    <div class="form-group">
                        <label for="footer_subtitle">Subtitle</label>
                        <textarea name="footer_subtitle" id="footer_subtitle" rows="3" class="form-control form-control-sm" placeholder="Subtitle"></textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('css')
<style>
    .btr {
        position: absolute;
        top: 5px;
        right: 5px;
        padding: 5px;
        background: white;
    }
    .table-projects-list td, .table-projects-list th {
        padding: 7px !important;
    }

    /* Slick Carousel */
    .slider-item {
        background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;
        position: relative;
    }
    .slider-item::before {
        position: absolute;
        content: "";
        height: 100%;
        width: 100%;
        background: rgba(0, 0, 0, 0);
        left: 0;
        top: 0;
    }
    .hero-area {
        background-size: cover;
        height: 300px !important;
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .hero-area:before {
        content: '';
        background: rgba(0, 0, 0, 0);
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }
    .hero-area .block {
        color: #fff;
    }
    .hero-area .block h1 {
        font-size: 60px;
        font-weight: 700;
        margin-bottom: 20px;
        text-transform: uppercase;
    }

    .hero-area .block p {
        color: #fff;
        width: 50%;
        margin-bottom: 20px;
    }

    .hero-area .block .btn-main {
        margin-right: 8px;
    }
    .hero-area .block .btn-main:hover {
        opacity: .8;
    }
    .hero-slider {
        overflow-x: hidden;
    }
    .hero-slider .prevArrow {
        left: -100px;
        background-image: url('{{asset('assets')}}/images/arrow-left.png');
        opacity: 0.8;
    }
    .hero-slider .nextArrow {
        right: -100px;
        background-image: url('{{asset('assets')}}/images/arrow-right.png');
        opacity: 0.8;
    }
    .hero-slider:hover .prevArrow {
        left: 20px;
    }

    .hero-slider:hover .nextArrow {
        right: 20px;
    }

    /* slick style */
    .slick-slide {
        outline: 0;
    }
    .slick-slide img {
        display: unset;
    }

    /* slick arrows */
    .slick-arrow {
        position: absolute;
        top: 50%;
        -webkit-transform: translateY(-50%);
        transform: translateY(-50%);
        z-index: 9;
        height: 36px;
        width: 36px;
        /* background: rgba(40, 171, 227, 0.5); */
        color: transparent;
        opacity: 0.8;
        border: 0;
        line-height: 70px;
        font-size: 35px;
        transition: .2s ease;
        border-radius: 50%;
    }
    .slick-arrow:focus {
        outline: 0;
    }
    .prevArrow {
        left: 0px;
    }
    .prevArrow::before {
        border-left: 2px solid #fff;
        border-top: 2px solid #fff;
        right: 35px;
    }
    .prevArrow::after {
        right: 20px;
    }
    .nextArrow {
        right: 0px;
    }
    .nextArrow::before {
        border-right: 2px solid #fff;
        border-bottom: 2px solid #fff;
        left: 35px;
    }
    .nextArrow::after {
        left: 20px;
    }
</style>
<!-- summernote -->
<link rel="stylesheet" href="{{asset('assets/ext')}}/summernote/summernote-bs4.min.css">
<!-- Themefisher Icon font -->
<link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/themefisher-font/style.css">
<!-- Slick Carousel -->
<link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/slick/slick.css">
@endpush

@push('js')
<script src="{{ asset('assets/ext/jquery-validation/jquery.validate.min.js') }}"></script>
<!-- Summernote -->
<script src="{{asset('assets/ext')}}/summernote/summernote-bs4.min.js"></script>
<script src="{{ asset('assets/ext') }}/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<!-- Slick Carousel -->
<script src="{{asset('assets/templates/bingo')}}/plugins/slick/slick.min.js"></script>
<script>
    var img = 0; 
    var aimg = 0; 
    $(document).ready(function () {
        $(".order").inputmask("decimal",{
            radixPoint:".",
            digits: 0,
            autoGroup: true,
            rightAlign: false,
            min: 0,
        });
        getSliders();
        getAbout();
        getCategoriesData();
        getProjectTitle();
        getProjectsData();
        getClientTitle();
        getClients();
        getServiceTitle();
        getServices();
        getContact();
        getSocmedData();
        getFooterTitle();
        // Summernote
        $('.summernote').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('#form-slider').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                image = $('#slider-image').val();
                slider_id = $('#slider_id').val();
                if (!image && !slider_id) {
                    Swal.fire('Attention!', 'Slider Image cannot be null!', 'error');
                    $('.saveButton').prop('disabled', false);
                } else {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function(data) {
                            $('.saveButton').prop('disabled', false);
                            if (data.success) {
                                Swal.fire('Great!', 'Slider Data has been saved!', 'success');
                                $('#modal-slider').modal('hide');
                            } else {
                                Swal.fire('Oh No!', 'Slider Data failed to save!', 'error');
                                $('#modal-slider').modal('hide');
                            }
                            getSliders();
                        },
                        error: function(err) {
                            $('.saveButton').prop('disabled', false);
                        }
                    });
                }
            }
        });
        $('#slider-image').change(function() {
            readURLSlider(this);
        })
        $('#form-about').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'About Us Data has been saved!', 'success');
                            $('#modal-about').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'About Us Data failed to save!', 'error');
                            $('#modal-about').modal('hide');
                        }
                        getAbout();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#about_image').change(function() {
            readURLAbout(this);
        });
        $('#about-images').change(function() {
            readURLAboutImages(this);
        });
        $('#form-project-title').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Project Title has been saved!', 'success');
                            $('#modal-project-title').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Project Title failed to save!', 'error');
                            $('#modal-project-title').modal('hide');
                        }
                        getProjectTitle();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#form-category').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Category Data has been saved!', 'success');
                            $('#modal-category').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Category Data failed to save!', 'error');
                            $('#modal-category').modal('hide');
                        }
                        getCategoriesData();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $("#category_name").keyup(function() {
            var Text = slugify($(this).val());
            $("#category_slug").val(Text);    
        });
        $('#form-project').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                image = $('#project-image').val();
                project_id = $('#project_id').val();
                if (!image && !project_id) {
                    Swal.fire('Attention!', 'Project Image cannot be null!', 'error');
                    $('.saveButton').prop('disabled', false);
                } else {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function(data) {
                            $('.saveButton').prop('disabled', false);
                            if (data.success) {
                                Swal.fire('Great!', 'Project Data has been saved!', 'success');
                                $('#modal-project').modal('hide');
                            } else {
                                Swal.fire('Oh No!', 'Project Data failed to save!', 'error');
                                $('#modal-project').modal('hide');
                            }
                            getProjectsData();
                        },
                        error: function(err) {
                            $('.saveButton').prop('disabled', false);
                        }
                    });
                }
            }
        });
        $('#project-image').change(function() {
            readURLProject(this);
        });
        $('#project-images').change(function() {
            readURLProjectImages(this);
        });
        $('#form-client-title').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Client Title has been saved!', 'success');
                            $('#modal-client-title').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Client Title failed to save!', 'error');
                            $('#modal-client-title').modal('hide');
                        }
                        getClientTitle();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#form-client').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                logo = $('#client_logo').val();
                client_id = $('#client_id').val();
                if (!logo && !client_id) {
                    Swal.fire('Attention!', 'Client Logo cannot be null!', 'error');
                } else {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function(data) {
                            $('.saveButton').prop('disabled', false);
                            if (data.success) {
                                Swal.fire('Great!', 'Client Data has been saved!', 'success');
                                $('#modal-client').modal('hide');
                            } else {
                                Swal.fire('Oh No!', 'Client Data failed to save!', 'error');
                                $('#modal-client').modal('hide');
                            }
                            getClients();
                        },
                        error: function(err) {
                            $('.saveButton').prop('disabled', false);
                        }
                    });
                }
            }
        });
        $('#client_logo').change(function() {
            readURLClient(this);
        });
        $('#form-service-title').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Service Title has been saved!', 'success');
                            $('#modal-service-title').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Service Title failed to save!', 'error');
                            $('#modal-service-title').modal('hide');
                        }
                        getServiceTitle();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#form-service').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                icon = $('#service_icon').val();
                service_id = $('#service_id').val();
                if (!icon && !service_id) {
                    Swal.fire('Attention!', 'Service Image cannot be null!', 'error');
                } else {
                    $.ajax({
                        url: form.action,
                        type: form.method,
                        data: new FormData(form),
                        processData: false,
                        contentType: false,
                        dataType: "json",
                        success: function(data) {
                            $('.saveButton').prop('disabled', false);
                            if (data.success) {
                                Swal.fire('Great!', 'Service has been saved!', 'success');
                                $('#modal-service').modal('hide');
                            } else {
                                Swal.fire('Oh No!', 'Service failed to save!', 'error');
                                $('#modal-service').modal('hide');
                            }
                            getServices();
                        },
                        error: function(err) {
                            $('.saveButton').prop('disabled', false);
                        }
                    });
                }
            }
        });
        $('#service_icon').change(function() {
            readURLService(this);
        });
        $('#form-contact').validate({
            rules: {
                contact_email: {
                    email: true
                }
            },
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Contact Us Title has been saved!', 'success');
                            $('#modal-contact').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Contact Us Title failed to save!', 'error');
                            $('#modal-contact').modal('hide');
                        }
                        getContact();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#form-socmed').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Social Media has been saved!', 'success');
                            $('#modal-socmed').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Social Media failed to save!', 'error');
                            $('#modal-socmed').modal('hide');
                        }
                        getSocmedData();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#form-footer-title').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Footer Title has been saved!', 'success');
                            $('#modal-footer-title').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Footer Title failed to save!', 'error');
                            $('#modal-footer-title').modal('hide');
                        }
                        getFooterTitle();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
    });

    var table_categories = $('#table_categories').DataTable( {
        responsive: true,
        processing: true,
        ajax: "",
        columns: [
            { data: "name" },
            { data: "slug" },
            { data: "description" },
            { 
                class: "text-center",
                render: function (data, type, row, meta) {
                    return '<a href="javascript:void(0);" onclick="editCategory('+row.id+');"><i class="fas fa-edit text-warning" title="Edit Category"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="removeCategory('+row.id+');"><i class="fas fa-trash text-danger" title="Remove Category"></i></a>';
                }
            },
        ],
        "aaSorting": []
    });

    function getCategoriesData() {
        table_categories.ajax.url("{{url('settings/getCategories')}}").load(null, false);
    }

    var table_projects = $('#table_projects').DataTable( {
        responsive: true,
        processing: true,
        ajax: "",
        columns: [
            { data: "desc" },
            { 
                class: "text-center",
                render: function (data, type, row, meta) {
                    return '<a href="javascript:void(0);" onclick="editProject('+row.id+');"><i class="fas fa-edit text-warning" title="Edit Project"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="removeProject('+row.id+');"><i class="fas fa-trash text-danger" title="Remove Project"></i></a>';
                }
            },
        ],
        "aaSorting": []
    });

    function getProjectsData() {
        table_projects.ajax.url("{{url('settings/getProjects')}}").load(null, false);
    }

    var table_socmed = $('#table_socmed').DataTable( {
        responsive: true,
        processing: true,
        ajax: "",
        columns: [
            { data: "name" },
            { data: "link" },
            { 
                class: "text-center",
                render: function (data, type, row, meta) {
                    return '<a href="javascript:void(0);"  onclick="editSocmed('+row.id+');"><i class="fas fa-edit text-warning" title="Edit Project"></i></a>&nbsp;&nbsp;<a href="javascript:void(0);" onclick="removeSocmed('+row.id+');"><i class="fas fa-trash text-danger" title="Remove Project"></i></a>';
                }
            },
        ],
        ordering: false,
        paging: false,
        info: false,
        searching: false,
    });

    function getSocmedData() {
        table_socmed.ajax.url("{{url('settings/getSocmeds')}}").load(null, false);
    }

    function clearForm() {
        $('#form-slider').trigger('reset');
        $('#slider_id').val('');
        $('#form-about').trigger('reset');
        $('#about_description').summernote('code', '');
        $('#form-category').trigger('reset');
        $('#category_id').val('');
        $('#form-projet-title').trigger('reset');
        $('#form-project').trigger('reset');
        $('#project_id').val('');
        $('#project-team').summernote('code', '');
        $('#project-description').summernote('code', '');
        $('#form-client-title').trigger('reset');
        $('#form-client').trigger('reset');
        $('#client_id').val('');
        $('#client_description').summernote('code', '');
        $('#form-service-title').trigger('reset');
        $('#form-service').trigger('reset');
        $('#service_id').val('');
        $('#form-contact').trigger('reset');
        $('#form-socmed').trigger('reset');
        $('#socmed_id').val('');
        $('.selectpicker').selectpicker('refresh');
        $('#project-images-list').html('');
        $('#slider-image_thumbnail').attr('src', "{{asset('assets/images/no-image.png')}}");
        $('#project-image_thumbnail').attr('src', "{{asset('assets/images/no-image.png')}}");
        $('#client_logo_thumbnail').attr('src', "{{asset('assets/images/no-image.png')}}");
        $('#service_icon_thumbnail').attr('src', "{{asset('assets/images/no-image.png')}}");
    }

    function getSliders() {
        $('#sliders-loader').show();
        $.get("{{url('settings/getSliders')}}", function(data) {
            $('#sliders-loader').hide();
            if (data) {
                $('#sliders').html(data);
            } else {
                $('#sliders').html('<div class="col-md-12"><p>There is no Sliders Here..</p></div>');
            }
        });
    }

    function addSlider() {
        clearForm();
        $('#slider-form-title').html('Add Slider');
        $('#modal-slider').modal('show');
    }

    function editSlider(id) {
        clearForm();
        $('#slider_id').val(id);
        $('#slider-form-title').html('Edit Slider');
        $.getJSON("{{url('settings/getSlider')}}/"+id, function(data) {
            $('#slider-title').val(data.title);
            $('#slider-subtitle').val(data.subtitle);
            $('#slider-image_thumbnail').attr('src', data.image_src);
            $('#modal-slider').modal('show');
        });
    }

    function removeSlider(id) {
        slider = $('.slider-item-db').length;
        if (slider > 1) {
            Swal.fire({
                title: "Are You Sure?",
                text: "Remove this Slider?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{url('settings/removeSlider')}}",
                        type: "post",
                        data: {_token: '{{csrf_token()}}', id: id},
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                Swal.fire('Great!', 'Slider successfully deleted!', 'success');
                                getSliders();
                            } else {
                                Swal.fire('Oh No!', 'Slider failed to delete', 'error');
                            }
                        },
                        error: function(err) {
                            Swal.fire('Error!', 'There is something wrong!', 'error');
                        }
                    });
                }
            });
        } else {
            Swal.fire('Warning!', 'The Slider must be more than 0!', 'error');
        }
    }
    
    function addSliderImage() {
        $('#slider-image').trigger('click');
    }

    function readURLSlider(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#slider-image').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    $('#slider-image_thumbnail').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function getAbout() {
        $('#about-loader').show();
        $.get("{{url('settings/getAbout')}}", function(data) {
            $('#about-loader').hide();
            $('#about').html(data);
            $('.hero-slider').slick({
                autoplay: true,
                infinite: true,
                arrows: true,
                prevArrow: '<button type=\'button\' class=\'prevArrow\'></button>',
                nextArrow: '<button type=\'button\' class=\'nextArrow\'></button>',
                dots: false,
                autoplaySpeed: 7000,
                pauseOnFocus: false,
                pauseOnHover: false
            });
            $('.hero-slider').slickAnimation();
        });
    }

    function editAbout() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#about_description').summernote('code', data.about_description);
            $('#about-images-list').html(data.about_images);
            aimg = data.about_image_key;
            $('#modal-about').modal('show');
        });
    }

    function addAboutImage() {
        $('#about_image').trigger('click');
    }

    function readURLAbout(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#about_image').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    $('#about_image_thumbnail').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function addAboutImages() {
        $('#about-images').trigger('click');
    }

    function readURLAboutImages(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#about-images').val();
                newVal = $('#about-images').next().val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!", "error");
                } else {
                    image = $('#about-images').clone();
                    image.attr('name', 'about_images['+aimg+']');
                    image.attr('id', 'about-images'+aimg);
                    image_div = '<div class="col-md-4" id="aimagediv'+aimg+'">'+
                        '<div class="card mb-2 bg-gradient-dark">'+
                            '<img class="img-fluid card-img-top" src="'+e.target.result+'" alt="Image'+aimg+'">'+
                            '<div class="card-img-overlay d-flex flex-column justify-content-end">'+
                                '<span class="btr">'+
                                    '<a href="javascript:void(0);" onclick="removeAboutImages('+aimg+')"><i class="fas fa-trash text-danger" title="Remove Image"></i></a>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
                    $('#about-images-list').append(image_div);
                    $('#about-images-list').append(image);
                    aimg++;
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function removeAboutImages(id) {
        Swal.fire({
            title: "Are You Sure?",
            text: "Remove this Image?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Remove it!"
        }).then((result) => {
            if (result.isConfirmed) {
                $('#aimagediv'+id).remove();
                $('#about-images'+id).remove();
            }
        });
    }

    function addCategory() {
        clearForm();
        $('#category-form-title').html('Add Category');
        $('#modal-category').modal('show');
    }

    function editCategory(id) {
        clearForm();
        $('#category_id').val(id);
        $('#category-form-title').html('Edit Category');
        $.getJSON("{{url('settings/getCategory')}}/"+id, function(data) {
            $('#category_name').val(data.name);
            $('#category_slug').val(data.slug);
            $('#category_description').summernote('code', data.description);
            $('#modal-category').modal('show');
        });
    }

    function slugify(string) {
        return string
            .toString()
            .trim()
            .toLowerCase()
            .replace(/\s+/g, "-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .replace(/^-+/, "")
            .replace(/-+$/, "");
    }

    function removeCategory(id) {
        Swal.fire({
            title: "Are You Sure?",
            text: "Remove this Category?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Remove it!"
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "{{url('settings/removeCategory')}}",
                    type: "post",
                    data: {_token: '{{csrf_token()}}', id: id},
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire('Great!', 'Category successfully deleted!', 'success');
                            getCategoriesData();
                        } else {
                            Swal.fire('Oh No!', 'Category failed to delete', 'error');
                        }
                    },
                    error: function(err) {
                        Swal.fire('Error!', 'There is something wrong!', 'error');
                    }
                });
            }
        });
    }

    function getProjectTitle() {
        $('#project-loader').show();
        $.get("{{url('settings/getProjectTitle')}}", function(data) {
            $('#project-loader').hide();
            $('#project-title').html(data);
        });
    }

    function editProjectTitle() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#project_title').val(data.project_title);
            $('#project_subtitle').val(data.project_subtitle);
        });
        $('#modal-project-title').modal('show');
    }

    function addProject() {
        clearForm();
        $('#project-form-title').html('Add Project');
        $.get("{{url('settings/getCategoryOptions')}}", function(data) {
            console.log(data);
            $('#project-category_id').html(data);
            $('#project-category_id').selectpicker('refresh');
            $('#modal-project').modal('show');
        });
    }

    function editProject(id) {
        clearForm();
        $('#project_id').val(id);
        $('#project-form-title').html('Edit Project');
        $.getJSON("{{url('settings/getProject')}}/"+id, function(data) {
            $('#project-category_id').html(data.categoryOptions);
            $('#project-category_id').selectpicker('refresh');
            $('#project-category_id').selectpicker('val', data.category_id);
            $('#project-name').val(data.name);
            $('#project-year').val(data.year);
            $('#project-location').val(data.location);
            $('#project-renderer').val(data.renderer);
            $('#project-order').val(data.order);
            $('#project-principal_id').selectpicker('val', data.principal_id);
            $('#project-ded_id').selectpicker('val', data.ded_id);
            $('#project-team').summernote('code', data.team);
            $('#project-description').summernote('code', data.description);
            $('#project-image_thumbnail').attr('src', data.image_src);
            $('#project-images-list').html(data.images_list);
            img = data.image_key;
            $('#modal-project').modal('show');
        });
    }

    function removeProject(id) {
        projects = table_projects.rows().count();
        if (projects > 1) {
            Swal.fire({
                title: "Are You Sure?",
                text: "Remove this Project?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{url('settings/removeProject')}}",
                        type: "post",
                        data: {_token: '{{csrf_token()}}', id: id},
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                Swal.fire('Great!', 'Project successfully deleted!', 'success');
                                getProjectsData();
                            } else {
                                Swal.fire('Oh No!', 'Project failed to delete', 'error');
                            }
                        },
                        error: function(err) {
                            Swal.fire('Error!', 'There is something wrong!', 'error');
                        }
                    });
                }
            });
        } else {
            Swal.fire('Warning!', 'The Project must be more than 0!', 'error');
        }
    }
    
    function addProjectImage() {
        $('#project-image').trigger('click');
    }

    function readURLProject(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#project-image').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    $('#project-image_thumbnail').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function addProjectImages() {
        $('#project-images').trigger('click');
    }

    function readURLProjectImages(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#project-images').val();
                newVal = $('#project-images').next().val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!", "error");
                } else {
                    if (input.files[0].size > 5242880) {
                        Swal.fire("Warning", "Inputed file cannot more than 5MB.", "error");
                    } else {
                        image = $('#project-images').clone();
                        image.attr('name', 'project_images['+img+']');
                        image.attr('id', 'project-images'+img);
                        image_div = '<div class="col-md-4" id="imagediv'+img+'">'+
                            '<div class="card mb-2 bg-gradient-dark">'+
                                '<img class="img-fluid card-img-top" src="'+e.target.result+'" alt="Image'+img+'">'+
                                '<div class="card-img-overlay d-flex flex-column justify-content-end">'+
                                    '<span class="btr">'+
                                        '<a href="javascript:void(0);" onclick="removeProjectImages('+img+')"><i class="fas fa-trash text-danger" title="Remove Image"></i></a>'+
                                    '</span>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
                        $('#project-images-list').append(image_div);
                        $('#project-images-list').append(image);
                        img++;
                    }
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function removeProjectImages(id) {
        Swal.fire({
            title: "Are You Sure?",
            text: "Remove this Image?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Remove it!"
        }).then((result) => {
            if (result.isConfirmed) {
                $('#imagediv'+id).remove();
                $('#project-images'+id).remove();
            }
        });
    }

    function getClientTitle() {
        $('#client-loader').show();
        $.get("{{url('settings/getClientTitle')}}", function(data) {
            $('#client-loader').hide();
            $('#client-title').html(data);
        });
    }

    function editClientTitle() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#client_title').val(data.client_title);
            $('#client_subtitle').val(data.client_subtitle);
        });
        $('#modal-client-title').modal('show');
    }

    function getClients() {
        $('#client-loader').show();
        $.get("{{url('settings/getClients')}}", function(data) {
            $('#client-loader').hide();
            $('#clients').html(data);
        });
    }

    function addClient() {
        clearForm();
        $('#client-form-title').html('Add Client');
        $('#modal-client').modal('show');
    }

    function editClient(id) {
        clearForm();
        $('#client_id').val(id);
        $('#client-form-title').html('Edit Client');
        $.getJSON("{{url('settings/getClient')}}/"+id, function(data) {
            $('#client_name').val(data.name);
            $('#client_description').summernote('code', data.description);
            $('#client_logo_thumbnail').attr('src', data.logo_src);
            $('#modal-client').modal('show');
        });
    }
    
    function addClientLogo() {
        $('#client_logo').trigger('click');
    }

    function removeClient(id) {
        clients = $('.client-item-db').length;
        if (clients > 1) {
            Swal.fire({
                title: "Are You Sure?",
                text: "Remove this Client?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{url('settings/removeClient')}}",
                        type: "post",
                        data: {_token: '{{csrf_token()}}', id: id},
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                Swal.fire('Great!', 'Client successfully deleted!', 'success');
                                getClients();
                            } else {
                                Swal.fire('Oh No!', 'Client failed to delete', 'error');
                            }
                        },
                        error: function(err) {
                            Swal.fire('Error!', 'There is something wrong!', 'error');
                        }
                    });
                }
            });
        } else {
            Swal.fire('Warning!', 'The Client must be more than 0!', 'error');
        }
    }

    function readURLClient(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#client_logo').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    $('#client_logo_thumbnail').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function getServiceTitle() {
        $('#service-loader').show();
        $.get("{{url('settings/getServiceTitle')}}", function(data) {
            $('#service-loader').hide();
            $('#service-title').html(data);
        });
    }

    function editServiceTitle() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#service_title').val(data.service_title);
            $('#service_subtitle').val(data.service_subtitle);
        });
        $('#modal-service-title').modal('show');
    }

    function addService() {
        clearForm();
        $('#service-form-title').html('Add Service');
        $('#modal-service').modal('show');
    }

    function editService(id) {
        clearForm();
        $('#service_id').val(id);
        $('#service-form-title').html('Edit Service');
        $.getJSON("{{url('settings/getService')}}/"+id, function(data) {
            $('#service_icon_thumbnail').attr('src', data.icon_src);
            $('.selectpicker').selectpicker('refresh');
            $('#service_name').val(data.name);
            $('#modal-service').modal('show');
        });
    }

    function removeService(id) {
        services = $('.service-item-db').length;
        if (services > 1) {
            Swal.fire({
                title: "Are You Sure?",
                text: "Remove this Service?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{url('settings/removeService')}}",
                        type: "post",
                        data: {_token: '{{csrf_token()}}', id: id},
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                Swal.fire('Great!', 'Service successfully deleted!', 'success');
                                getServices();
                            } else {
                                Swal.fire('Oh No!', 'Service failed to delete', 'error');
                            }
                        },
                        error: function(err) {
                            Swal.fire('Error!', 'There is something wrong!', 'error');
                        }
                    });
                }
            });
        } else {
            Swal.fire('Warning!', 'The Service must be more than 0!', 'error');
        }
    }

    function getServices() {
        $('#service-loader').show();
        $.get("{{url('settings/getServices')}}", function(data) {
            $('#service-loader').hide();
            $('#services').html(data);
        });
    }
    
    function addServiceIcon() {
        $('#service_icon').trigger('click');
    }

    function readURLService(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#service_icon').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    $('#service_icon_thumbnail').attr('src', e.target.result);
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function getContact() {
        $('#contact-loader').show();
        $.get("{{url('settings/getContact')}}", function(data) {
            $('#contact-loader').hide();
            $('#contact').html(data);
        });
    }

    function editContact() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#contact_email').val(data.contact_email);
            $('#contact_title').val(data.contact_title);
            $('#contact_subtitle').val(data.contact_subtitle);
            $('#contact_map').val(data.contact_map);
            $('#contact_description').summernote('code', data.contact_description);
        });
        $('#modal-contact').modal('show');
    }

    function addSocMed() {
        clearForm();
        $('#socmed-form-title').html('Add Service');
        $('#modal-socmed').modal('show');
    }

    function editSocmed(id) {
        clearForm();
        $('#socmed_id').val(id);
        $.getJSON("{{url('settings/getSocmed')}}/"+id, function(data) {
            $('#socmed_name').val(data.name);
            $('#socmed_link').val(data.link);
            $('#socmed_icon').val(data.icon);
            $('.selectpicker').selectpicker('refresh');
        });
        $('#modal-socmed').modal('show');
    }

    function removeSocmed(id) {
        clients = $('.client-item-db').length;
        if (clients > 1) {
            Swal.fire({
                title: "Are You Sure?",
                text: "Remove this Social Media?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Remove it!"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        url: "{{url('settings/removeSocmed')}}",
                        type: "post",
                        data: {_token: '{{csrf_token()}}', id: id},
                        dataType: "json",
                        success: function(data) {
                            if (data.success) {
                                Swal.fire('Great!', 'Social Media successfully deleted!', 'success');
                                getSocmedData();
                            } else {
                                Swal.fire('Oh No!', 'Social Media failed to delete', 'error');
                            }
                        },
                        error: function(err) {
                            Swal.fire('Error!', 'There is something wrong!', 'error');
                        }
                    });
                }
            });
        } else {
            Swal.fire('Warning!', 'The Social Media must be more than 0!', 'error');
        }
    }

    function editFooterTitle() {
        clearForm();
        $.getJSON("{{url('settings/getSetting')}}", function(data) {
            $('#footer_title').val(data.footer_title);
            $('#footer_subtitle').val(data.footer_subtitle);
        });
        $('#modal-footer-title').modal('show');
    }

    function getFooterTitle() {
        $('#footer-loader').show();
        $.get("{{url('settings/getFooterTitle')}}", function(data) {
            $('#footer-loader').hide();
            $('#footer-title').html(data);
        });
    }
    
    function isImage(ext) {
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'jpeg':
            case 'bmp':
            case 'png':
            return true;
        }
        return false;
    }
</script>
@endpush