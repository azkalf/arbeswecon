@extends('layouts.bingo')
@section('title', 'Home')

@section('content')
<!-- Start Services Section
==================================== -->
<section class="services section bg-dark" id="services">
	<div class="container">
		<div class="row no-gutters">
			@foreach ($services as $service)
			<!-- Single Service Item -->
			<div class="col">
				<div class="service-block p-4 text-center">
					<div class="service-icon text-center">
						<a href="{{url('/contact')}}"><img src="{{asset('storage/services/file_' . $service->icon . '?' . date('His'))}}" alt="{{$service->name}}" class="img-fluid"></a>
					</div>
					<a href="{{url('/contact')}}"><h3>{{$service->name}}</h3></a>
				</div>
			</div>
			<!-- End Single Service Item -->
			@endforeach
		</div> <!-- End row -->
	</div> <!-- End container -->
</section> <!-- End section -->
@endsection