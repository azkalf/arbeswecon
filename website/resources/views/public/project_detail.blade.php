@extends('layouts.bingo')
@section('title', $project->name)

@section('content')
{{-- <section class="single-page-header" style="background-image: url('{{asset('storage/projects/file_'.$project->image)}}')">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Project 1</h2>
                <ol class="breadcrumb header-bradcrumb justify-content-center">
					<li class="breadcrumb-item"><a href="{{url('/')}}" class="text-white">Home</a></li>
					<li class="breadcrumb-item"><a href="{{url('projects')}}" class="text-white">Projects</a></li>
					<li class="breadcrumb-item active" aria-current="page">{{$project->name}}</li>
				</ol>
            </div>
        </div>
    </div>
</section> --}}

<!-- blog details part start -->
<section class="blog-details section bg-dark">
    <div class="container">
        <div class="col-md-6 float-right">
            <article class="post">
                <div class="hero-slider">
                    <div class="slider-item th-fullpage hero-area" style="background-image: url({{asset('storage/projects/file_'.$project->image)}});">
                    </div>
                    @foreach ($project->images as $image)
                    <div class="slider-item th-fullpage hero-area" style="background-image: url({{asset('storage/projects/file_'.$image->image)}});">
                    </div>
                    @endforeach
                </div>
                <div id="justGallery">
                    <a href="{{asset('storage/projects/file_'.$project->image)}}" class="swipe">
                        <img src="{{asset('storage/projects/thumb_'.$project->image)}}"/>
                    </a>
                    @foreach ($project->images as $image)
                    <a href="{{asset('storage/projects/file_'.$image->image)}}" class="swipe">
                        <img src="{{asset('storage/projects/thumb_'.$image->image)}}"/>
                    </a>
                    @endforeach
                </div>
            </article>
        </div>
        <div class="col-md-6 float-left">
            <article class="post">
                <!-- Post Content -->
                <div class="post-content">
                    <table>
                        <tr>
                            <td width="120">Project Name</td>
                            <td width="10">:</td>
                            <td>{{$project->name}}</td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>:</td>
                            <td>{{$project->location}}</td>
                        </tr>
                        <tr>
                            <td>Year</td>
                            <td>:</td>
                            <td>{{$project->year}}</td>
                        </tr>
                        <tr>
                            <td>Team</td>
                            <td>:</td>
                            <td>{{$project->team}}</td>
                        </tr>
                    </table><br>
                    <p>
                        {!!$project->description!!}
                    </p>
                </div>
                <hr>
                <div class="row mb-5">
                    <div class="col-6 text-left">
                        @isset($prevProject)
                            <a href="{{url('project/'.$prevProject->id)}}">
                                Prev<br><span class="d-none d-md-block">{{$prevProject->name}}</span>
                            </a>
                        @endisset
                    </div>
                    <div class="col-6 text-right">
                        @isset($nextProject)
                            <a href="{{url('project/'.$nextProject->id)}}">
                                Next<br><span class="d-none d-md-block">{{$nextProject->name}}</span>
                            </a>
                        @endisset
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>
<!-- blog details part end -->
@endsection

@push('css')
<style>
    .hero-area {
        height: 300px !important;
    }
    @media (min-width: 768px) {
        .col-md-6.float-right {
            padding-right: 0px;
        }
        .col-md-6.float-left {
            padding-left: 0px;
        }
    }
</style>
<link rel="stylesheet" href="{{asset('assets/ext')}}/justifiedGallery/css/justifiedGallery.css">
<link rel="stylesheet" href="{{asset('assets/ext')}}/swipebox/css/swipebox.css">
@endpush

@push('js')
<script src="{{ asset('assets/ext/justifiedGallery/js/jquery.justifiedGallery.js') }}"></script>
<script src="{{ asset('assets/ext/swipebox/js/jquery.swipebox.js') }}"></script>
<script>
    $("#justGallery").justifiedGallery({
        rowHeight : 70,
        margins: 4,
    }).on('jg.complete', function () {
        $('.swipe').swipebox();
    });
</script>
@endpush