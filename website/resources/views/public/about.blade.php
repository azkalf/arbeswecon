@extends('layouts.bingo')
@section('title', 'Project 1')

@section('content')
<!-- blog details part start -->
<section class="blog-details section bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="hero-slider">
                    @foreach ($images as $image)
                    <div class="slider-item th-fullpage hero-area" style="background-image: url({{asset('storage/abouts/file_'.$image->image)}});">
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-6">
                {!!$setting->about_description!!}
            </div>
        </div>
    </div>
</section>
<!-- blog details part end -->
@endsection

@push('css')
<style>
    .hero-area {
        height: 300px !important;
    }
</style>
@endpush

@push('js')
@endpush