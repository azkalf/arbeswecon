@extends('layouts.bingo')
@section('title', 'Employee Detail')

@section('content')
<!-- blog details part start -->
<section class="blog-details section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="post-image">
                    <a href="{{$employee->photo_url}}" class="swipe">
                        <img class="img-fluid w-100" src="{{$employee->photo_url}}" alt="post-image">
                    </a>
                </div>
                <!-- Post Content -->
            </div>
            <div class="col-sm-8">
                <div class="post-content">
                    <h3>{{$employee->fullname}} ({{$employee->nickname}})</h3>
                    <strong>ID Card:</strong> {{$employee->idcard}}<br>
                    <strong>Job Position:</strong> {{$employee->position}}<br>
                    {!!$employee->description!!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog details part end -->
@endsection

@push('css')
<link rel="stylesheet" href="{{asset('assets/ext')}}/swipebox/css/swipebox.css">
@endpush

@push('js')
<script src="{{ asset('assets/ext/swipebox/js/jquery.swipebox.js') }}"></script>
<script>
    $('.swipe').swipebox();
</script>
@endpush