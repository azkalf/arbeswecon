@php
    $category_name = isset($category) ? $category->name : '';
@endphp
@extends('layouts.bingo')
@section('title', $category_name.' Projects')

@section('content')
<section class="posts section bg-dark">
	<div class="container">
		<div class="row">
            <div class="col-md-3 d-none d-md-block">
                <!-- sidebar -->
                <aside class="sidebar">
                    <div class="widget-categories widget">
                        <h2>Project Categories</h2>
                        <!-- widget categories list -->
                        <ul class="widget-categories-list">
                            <li>
                                <a href="{{url('projects')}}">All Projects</a>
                            </li>
                            @foreach ($categories as $category)
                            <li>
                                <a href="{{url('projects/'.$category->slug)}}">{{$category->name}}</a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
			<div class="col-md-9 col-sm-12">
				<div class="row">
					@foreach ($projects as $project)
					<div class="col-md-3 col-6 filtr-item" data-category="">
						<div class="portfolio-block">
							<img class="img-fluid" src="{{asset('storage/projects/thumb_'.$project->image)}}" alt="">
							<div class="caption">
								<h4><a href="{{url('/project/'.$project->id)}}">Detail</a></h4>
							</div>
							<p class="text-center" style="font-size: 10px;"><a href="{{url('/project/'.$project->id)}}" class="text-white">{{$project->name}}</a></p>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
@endsection