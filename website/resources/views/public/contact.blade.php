@extends('layouts.bingo')
@section('title', 'Home')

@section('content')
<!--Start Contact Us
=========================================== -->		
<section class="contact-us bg-dark" id="contact">
	<div class="container">
		<div class="row">
			<!-- Contact Form -->
			<div class="contact-form col-md-6 " >
				<form id="contact-form" method="post" role="form">
					@csrf
					<div class="form-group">
						<input type="text" placeholder="Your Name" class="form-control" name="name" id="name">
					</div>
					
					<div class="form-group">
						<input type="email" placeholder="Your Email" class="form-control" name="email" id="email">
					</div>
					
					<div class="form-group">
						<input type="text" placeholder="Subject" class="form-control" name="subject" id="subject">
					</div>
					
					<div class="form-group">
						<textarea rows="6" placeholder="Message" class="form-control" name="message" id="message"></textarea>	
					</div>
					
					<div id="success" class="success">
						Thank you. The Mailman is on His Way :)
					</div>
					
					<div id="error" class="error">
						Sorry, don't know what happened. Try later :(
					</div>
					<div id="cf-submit">
						<input type="submit" id="contact-submit" class="btn btn-transparent" value="Submit">
					</div>						
					
				</form>
			</div>
			<!-- Contact Details -->
			<div class="contact-details col-md-6 text-right" >
				{!!$setting->contact_map!!}<br>
				{!!$setting->contact_description!!}
				<!-- Footer Social Links -->
				<div class="social-icon">
					<ul>
						@foreach ($socmeds as $socmed)
						<li><a href="{{$socmed->link}}"><i class="{{$socmed->icon}}"></i></a></li>
						@endforeach
					</ul>
				</div>
				<!--/. End Footer Social Links -->
			</div>
			<!-- / End Contact Details -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end section -->
@endsection

@push('js')
<script>
	$('#contact-form').validate({
		rules: {
			name: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			subject: {
				required: true
			},
			message: {
				required: true
			}
		},
		messages: {
			name: {
				required: 'Come on, you have a name don\'t you?',
				minlength: 'Your name must consist of at least 2 characters'
			},
			email: {
				required: 'Please put your email address!'
			},
			subject: {
				required: 'Please fill out the subject!'
			},
			message: {
				required: 'Put some messages here..',
			}
		},
		submitHandler: function (form) {
			$(form).ajaxSubmit({
				type: 'POST',
				data: $(form).serialize(),
				url: '{{url('sendmail')}}',
				success: function (status) {
					if (status == 'success') {
						$('#contact-form #success').fadeIn();
						$('#contact-form #error').fadeOut();
						$('#contact-form').trigger('reset');
					} else {
						$('#contact-form #error').fadeIn();
						$('#contact-form #success').fadeOut();
					}
				},
				error: function () {
					$('#contact-form #error').fadeIn();
					$('#contact-form #success').fadeOut();
				}
			});
		}
	});
</script>
@endpush