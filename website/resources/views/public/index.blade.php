@extends('layouts.bingo')
@section('title', 'Home')

@section('content')
<section class="home" id="home">
	<div class="hero-slider">
		@foreach ($sliders as $slider)
		<div class="slider-item th-fullpage hero-area" style="background-image: url({{asset('storage/sliders/file_'.$slider->image)}});">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						{{-- <h1 data-duration-in=".3" data-animation-in="fadeInUp" data-delay-in=".1">{{$slider->title}}</h1> --}}
						<p data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".5">{{$slider->title}}</p>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</section>
{{-- 
@if($setting->about_title != '')
<!-- Start About Section
==================================== -->
<section class="about" id="about">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<!-- section title -->
				<div class="title text-center"  >
					<h2>{{$setting->about_title}}</h2>
					<p>{{$setting->about_subtitle}}</p>
					<div class="border"></div>
				</div>
				<!-- /section title -->
			</div>

			<div class="col-md-6">
				<img src="{{asset('storage/settings/file_'.$setting->about_image)}}" class="img-fluid" alt="">
			</div>
			<div class="col-md-6">
				{!!$setting->about_description!!}
			</div>
		</div> 		<!-- End row -->
	</div>   	<!-- End container -->
</section>   <!-- End section -->
@endif
@if($setting->project_title != '')
<!-- Start Portfolio Section
=========================================== -->
<section class="portfolio section" id="portfolio">
	<div class="container-fluid">
		<div class="row ">
			<div class="col-lg-12">
				<!-- section title -->
				<div class="title text-center">
					<h2>{{$setting->project_title}}</h2>
					<p>{{$setting->project_subtitle}}</p>
					<div class="border"></div>
				</div>
				<div class="row filtr-container">
					@foreach ($projects as $project)
					<div class="col-md-3 col-sm-6 col-xs-6 filtr-item" data-category="">
						<div class="portfolio-block">
							<img class="img-fluid" src="{{asset('storage/projects/thumb_'.$project->image)}}" alt="">
							<div class="caption">
								<a class="search-icon" href="{{url('/project/'.$project->id)}}">
									<i class="tf-ion-ios-search-strong"></i>
								</a>
								<h4><a href="{{url('/project/'.$project->id)}}">{{$project->name}}</a></h4>
							</div>
						</div>
					</div>
					@endforeach
				</div>
				<div class="text-center">
					<div class="border"></div>
					<a href="{{url('projects')}}" class="btn btn-primary">All Projects</a>
				</div>
			</div> <!-- /end col-lg-12 -->
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- End section -->
@endif
@if($setting->client_title != '')
<!-- Start Team Skills
=========================================== -->
<section class="team-skills section" id="skills">
	<div class="container">
		<div class="row">
			<!-- section title -->
			<div class="col-12">
				<div class="title text-center">
					<h2>{{$setting->client_title}}</h2>
					<p>{{$setting->client_subtitle}}</p>
					<div class="border"></div>
				</div>
			</div>
			<!-- /section title -->
		</div> <!-- End row -->
		<div class="row">
			<div class="col-md-12">
				<div id="clients-slider" class="clients-logo-slider">
					@foreach ($clients as $client)
					<img src="{{asset('storage/clients/file_'.$client->logo)}}" alt="{{$client->name}}">
					@endforeach
				</div>
			</div>
		</div>
	</div> <!-- End container -->
</section> <!-- End section -->
@endif
@if($setting->service_title != '')
<!-- Start Services Section
==================================== -->
<section class="services" id="services">
	<div class="container">
		<div class="row no-gutters">
			<!-- section title -->
			<div class="col-12">
				<div class="title text-center">
					<h2>{{$setting->service_title}}</h2>
					<p>{{$setting->service_subtitle}}</p>
					<div class="border"></div>
				</div>
			</div>
			<!-- /section title -->
			@php
				$count = 0;
			@endphp
			@foreach ($services as $service)
			<!-- Single Service Item -->
			<div class="col-lg-4 col-sm-6 mb-4 mb-lg-0">
				<div class="service-block p-4 {{++$count%2 ? 'color-bg' : ''}} text-center">
					<div class="service-icon text-center">
						<i class="{{$service->icon}}"></i>
					</div>
					<h3>{{$service->name}}</h3>
					<p>{{$service->description}}</p>
				</div>
			</div>
			<!-- End Single Service Item -->
			@endforeach
		</div> <!-- End row -->
	</div> <!-- End container -->
</section> <!-- End section -->
@endif
@if ($setting->contact_title != '')
<!--Start Contact Us
=========================================== -->		
<section class="contact-us" id="contact">
	<div class="container">
		<div class="row">
			<!-- section title -->
			<div class="col-12">
				<div class="title text-center" >
					<h2>{{$setting->contact_title}}</h2>
					<p>{{$setting->contact_subtitle}}</p>
					<div class="border"></div>
				</div>
			</div>
			<!-- /section title -->
			<div class="col-12 map">
				{!!$setting->contact_map!!}
			</div>
			<!-- Contact Details -->
			<div class="contact-details col-md-6 " >
				{!!$setting->contact_description!!}
				<!-- Footer Social Links -->
				<div class="social-icon">
					<ul>
						@foreach ($socmeds as $socmed)
						<li><a href="{{$socmed->link}}"><i class="{{$socmed->icon}}"></i></a></li>
						@endforeach
					</ul>
				</div>
				<!--/. End Footer Social Links -->
			</div>
			<!-- / End Contact Details -->
				
			<!-- Contact Form -->
			<div class="contact-form col-md-6 " >
				<form id="contact-form" method="post" role="form">
					<div class="form-group">
						<input type="text" placeholder="Your Name" class="form-control" name="name" id="name">
					</div>
					
					<div class="form-group">
						<input type="email" placeholder="Your Email" class="form-control" name="email" id="email">
					</div>
					
					<div class="form-group">
						<input type="text" placeholder="Subject" class="form-control" name="subject" id="subject">
					</div>
					
					<div class="form-group">
						<textarea rows="6" placeholder="Message" class="form-control" name="message" id="message"></textarea>	
					</div>
					
					<div id="success" class="success">
						Thank you. The Mailman is on His Way :)
					</div>
					
					<div id="error" class="error">
						Sorry, don't know what happened. Try later :(
					</div>
					<div id="cf-submit">
						<input type="submit" id="contact-submit" class="btn btn-transparent" value="Submit">
					</div>						
					
				</form>
			</div>
			<!-- ./End Contact Form -->
		
		</div> <!-- end row -->
	</div> <!-- end container -->
</section> <!-- end section -->
@endif --}}
@endsection