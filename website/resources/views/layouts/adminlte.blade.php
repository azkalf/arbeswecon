<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') - {{ config('app.name', 'Arbe Admin') }}</title>
	<!-- Favicon icon -->
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/favicon.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/images/favicon.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/images/favicon.png')}}">

	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="{{asset('assets/templates/adminlte')}}/plugins/fontawesome-free/css/all.min.css">
	<!-- overlayScrollbars -->
	<link rel="stylesheet" href="{{asset('assets/templates/adminlte')}}/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
	<link type="text/css" href="{{ asset('assets/ext') }}/datatables/datatables.min.css" rel="stylesheet">
	<link href="{{ asset('assets/ext') }}/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
	<link href="{{ asset('assets/ext') }}/sweetalert2/sweetalert2.min.css" rel="stylesheet">
	<link href="{{ asset('assets/ext') }}/materialicon/material-icons.css" rel="stylesheet">
	@stack('css')
	<!-- Theme style -->
	<link rel="stylesheet" href="{{asset('assets/templates/adminlte')}}/dist/css/adminlte.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed text-sm">
<div class="wrapper">
	<!-- Navbar -->
	<nav class="main-header navbar navbar-expand navbar-white navbar-light">
		<!-- Left navbar links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
			</li>
			<li class="nav-item d-none d-sm-inline-block">
				<a href="javascrpit:void(0);" class="nav-link">Arbe Swecon International</a>
			</li>
		</ul>

		<!-- Right navbar links -->
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" role="button">
					<i class="fas fa-key mr-1 text-dark"></i> Keluar
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
					@csrf
				</form>
			</li>
		</ul>
	</nav>
	<!-- /.navbar -->

	<!-- Main Sidebar Container -->
	<aside class="main-sidebar sidebar-dark-primary elevation-4">
		<!-- Brand Logo -->
		<a href="{{url('/home')}}" class="brand-link">
			<img src="{{asset('assets/images/logo-white.png')}}" alt="Arbe Swecon International" class="brand-image" style="opacity: .8">
			<span class="brand-text font-weight-light"><b>Arbe</b>Admin</span>
		</a>
		@php
			$user = auth()->user();
		@endphp
		<!-- Sidebar -->
		<div class="sidebar">
			<!-- Sidebar user panel (optional) -->
			<div class="user-panel pt-3 pb-3 mb-3 d-flex">
				<div class="image">
					<img src="{{asset('assets/images/user.jpg')}}" class="img-circle" alt="User Image" />
				</div>
				<div class="info">
					<a href="javascript:void(0);" class="d-block">{{$user->name}}<br>{{$user->email}}</a>
				</div>
			</div>
			<!-- Sidebar Menu -->
			<nav class="mt-2">
				<ul class="nav nav-pills nav-sidebar flex-column nav-flat nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
					<li class="nav-item">
						<a href="{{url('/home')}}" class="nav-link {{active_url('home')}}">
							<i class="nav-icon fas fa-home"></i>
							<p>Home</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('/employees')}}" class="nav-link {{active_url('employees')}}">
							<i class="nav-icon fas fa-users"></i>
							<p>Eemployee</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('/settings')}}" class="nav-link {{active_url('settings')}}">
							<i class="nav-icon fas fa-cogs"></i>
							<p>Page Setting</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="{{url('/inbox')}}" class="nav-link {{active_url('inbox')}}">
							<i class="nav-icon fas fa-envelope"></i>
							<p>Inbox <span class="right badge badge-danger hidden" id="badge-inbox">0</span></p>
						</a>
					</li>
				</ul>
			</nav>
			<!-- /.sidebar-menu -->
		</div>
		<!-- /.sidebar -->
	</aside>

	<!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<div class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1 class="m-0 text-dark">@yield('title')</h1>
					</div><!-- /.col -->
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							@yield('breadcrumb')
						</ol>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.container-fluid -->
		</div>
		<!-- /.content-header -->

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				@yield('content')
			</div><!--/. container-fluid -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Control sidebar content goes here -->
	</aside>
	<!-- /.control-sidebar -->

	<!-- Main Footer -->
	<footer class="main-footer">
		<strong>Copyright &copy; {{date('Y')}} <a href="javascript:void(0);">Arbe Swecon International</a>.</strong>
		<div class="float-right d-none d-sm-inline-block">
			<b>Arbe</b>Admin <b>1</b>.0
		</div>
	</footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="{{asset('assets/templates/adminlte')}}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="{{asset('assets/templates/adminlte')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{asset('assets/templates/adminlte')}}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/templates/adminlte')}}/dist/js/adminlte.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{asset('assets/templates/adminlte')}}/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="{{asset('assets/templates/adminlte')}}/plugins/raphael/raphael.min.js"></script>
<script src="{{asset('assets/templates/adminlte')}}/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="{{asset('assets/templates/adminlte')}}/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="{{asset('assets/templates/adminlte')}}/plugins/chart.js/Chart.min.js"></script>
{{-- Bootstrap Select --}}
<script src="{{ asset('assets/ext') }}/bootstrap-select/js/bootstrap-select.min.js"></script>
{{-- Sweetalert2 --}}
<script src="{{ asset('assets/ext') }}/sweetalert2/sweetalert2.all.min.js"></script>
{{-- Datatables --}}
<script src="{{ asset('assets/ext') }}/datatables/datatables.min.js"></script>
<script>
	$(document).ready(function() {
		updateBadge('inbox');
	})

	function updateBadge(param) {
		$.ajax({
			url: "{{url('badge/update')}}",
			data: {param: param},
			dataType: "json",
			success: function(data) {
				$('#badge-'+param).html(data);
				if (data > 0) {
					$('#badge-'+param).show();
				} else {
					$('#badge-'+param).hide();
				}
			},
			error: function(err) {
				console.log(err)
			}
		});
	}
</script>
@stack('js')
</body>
</html>
