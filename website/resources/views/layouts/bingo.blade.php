<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html> <!--<![endif]-->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="description" content="PT. Arbe Swecon International">
  <meta name="author" content="arbeswecon.co.id">
  <title>@yield('title') | Arbe Swecon International</title>
  <!-- Mobile Specific Meta
  ================================================== -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Favicon -->
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/favicon.png')}}" />
  <!-- CSS
  ================================================== -->
  <!-- Themefisher Icon font -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/themefisher-font/style.css">
  <!-- bootstrap.min css -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/bootstrap/css/bootstrap.min.css">
  <!-- Lightbox.min css -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/lightbox2/dist/css/lightbox.min.css">
  <!-- animation css -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/animate/animate.css">
  <!-- Slick Carousel -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/plugins/slick/slick.css">
  @stack('css')
  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="{{asset('assets/templates/bingo')}}/css/style.css?{{date('YmdHis')}}">  
</head>
@php
    $setting = App\Models\Setting::first();
    $categories = App\Models\Category::all();
@endphp
<body id="body">
  <!--
  Start Preloader
  ==================================== -->
  <div id="preloader">
    <div class='preloader'>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div> 
  <!--
  End Preloader
  ==================================== -->
  <!--
  Fixed Navigation
  ==================================== -->
  <header class="navigation fixed-top">
    <div>
      <!-- main nav -->
      <nav class="navbar navbar-expand-lg navbar-light">
        <!-- logo -->
        <a class="navbar-brand logo" href="{{url('/')}}">
          <img class="logo-default" src="{{asset('assets/images/logo-white.png')}}" alt="logo"/>
          <img class="logo-white" src="{{asset('assets/images/logo-white.png')}}" alt="logo"/>
        </a>
        <!-- /logo -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation"
          aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navigation">
          <ul class="navbar-nav ml-auto text-center">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="{{url('/projects')}}" id="projectDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Projects</a>
              <div class="dropdown-menu" aria-labelledby="projectDropdown">
                <a class="dropdown-item" href="{{url('/projects')}}">All Projects</a>
                @foreach ($categories as $category)
                <a class="dropdown-item" href="{{url('/projects/'.$category->slug)}}">{{$category->name}}</a>
                @endforeach
              </div>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{url('/about-us')}}">About Us</a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{url('/services')}}">Services</a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" href="{{url('/contact')}}">Contact</a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- /main nav -->
    </div>
  </header>
  <!--
  End Fixed Navigation
  ==================================== -->
  @yield('content'){{-- 
  <!--====  End of Google Map  ====-->
  <footer id="footer" class="bg-one">
    <div class="top-footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-6">
            <h3>{{$setting->footer_title}}</h3>
            <p>{{$setting->footer_subtitle}}</p>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-6">
            <ul>
              <li><h3>Connect with us Socially</h3></li>
              @foreach (socmeds() as $socmed)
              <li><a href="{{$socmed->link}}">{{$socmed->name}}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
      </div> <!-- end container -->
    </div>
    <div class="footer-bottom">
      <h5>Copyright &copy;{{date('Y')}}. All rights reserved.</h5>
      <h6>Arbe Swecon International <a href="">arbeswecon.co.id</a></h6>
    </div>
  </footer> <!-- end footer -->
  <!-- end Footer Area
  ========================================== -->
 --}}
  <!-- 
  Essential Scripts
  =====================================-->
  <!-- Main jQuery -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/jquery/jquery.min.js"></script>
  <!-- Form Validation -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/form-validation/jquery.form.js"></script> 
  <script src="{{asset('assets/templates/bingo')}}/plugins/form-validation/jquery.validate.min.js"></script>
  <!-- Bootstrap4 -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/bootstrap/js/bootstrap.min.js"></script>
  <!-- Parallax -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/parallax/jquery.parallax-1.1.3.js"></script>
  <!-- lightbox -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/lightbox2/dist/js/lightbox.min.js"></script>
  <!-- Owl Carousel -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/slick/slick.min.js"></script>
  <!-- filter -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/filterizr/jquery.filterizr.min.js"></script>
  <!-- Smooth Scroll js -->
  <script src="{{asset('assets/templates/bingo')}}/plugins/smooth-scroll/smooth-scroll.min.js"></script>
  <!-- Custom js -->
  <script src="{{asset('assets/templates/bingo')}}/js/script.js?{{date('YmdHis')}}"></script>
  @stack('js')
</body>
</html>