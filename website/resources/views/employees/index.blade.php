@extends('layouts.adminlte')
@section('title', 'Employees')

@section('content')
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title mt-1">Employee List</h3>
        <button class="btn btn-sm btn-light float-right" onclick="addEmployee();">Add Employee</button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered" id="employee_table">
                <thead>
                    <tr>
                        <th class="text-center" width="10">#</th>
                        <th class="text-center" width="10">Photo</th>
                        <th class="text-center">ID Card</th>
                        <th class="text-center">Nickname</th>
                        <th class="text-center">FullName</th>
                        <th class="text-center">Position</th>
                        <th class="text-center">Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Form Add Employee --}}
<div class="modal fade" id="modal-employee">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="employee-form-title">Add Employee</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{url('employees/save')}}" id="form-employee" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="employee_id" id="employee_id">
                <div class="modal-body">
                    <input type="file" name="photo" id="photo" style="display: none;">
                    <img src="{{asset('assets/images/user.jpg')}}" id="photo_image" onclick="addPhoto();" height="100px" style="cursor: pointer;">
                    <div class="form-group">
                        <label for="idcard">ID Card Number <span class="text-danger">*</span></label>
                        <input type="text" name="idcard" class="form-control" id="idcard" placeholder="ID Card Number" required>
                    </div>
                    <div class="form-group">
                        <label for="fullname">Fullname <span class="text-danger">*</span></label>
                        <input type="text" name="fullname" class="form-control datepicker" id="fullname" placeholder="Fullname" required>
                    </div>
                    <div class="form-group">
                        <label for="nickname">Nickname <span class="text-danger">*</span></label>
                        <input type="text" name="nickname" class="form-control datepicker" id="nickname" placeholder="Nickname" required>
                    </div>
                    <div class="form-group">
                        <label for="position">Job Position</label>
                        <input type="text" name="position" class="form-control datepicker" id="position" placeholder="Job Position">
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" rows="3" class="form-control form-control-sm summernote" placeholder="Description" required>
                        </textarea>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary saveButton">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('css')
<!-- summernote -->
<link rel="stylesheet" href="{{asset('assets/ext')}}/summernote/summernote-bs4.min.css">
@endpush

@push('js')
<script src="{{ asset('assets/ext/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/ext/jquery-validation/localization/messages_id.min.js') }}"></script>
<!-- Summernote -->
<script src="{{asset('assets/ext')}}/summernote/summernote-bs4.min.js"></script>
<script>
    $(document).ready(function () {
        getEmployeeData();
        // Summernote
        $('.summernote').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });
        $('#form-employee').validate({
            highlight: function (input) {
                $(input).addClass('is-invalid');
            },
            unhighlight: function (input) {
                $(input).removeClass('is-invalid');
            },
            errorPlacement: function( error, element ) {
                var placement = element.closest('.form-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.append(error);
                }
                console.log(error, placement);
            },
            submitHandler: function(form) {
                $('.saveButton').prop('disabled', true);
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: new FormData(form),
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(data) {
                        $('.saveButton').prop('disabled', false);
                        if (data.success) {
                            Swal.fire('Great!', 'Employee Data has been saved!', 'success');
                            $('#modal-employee').modal('hide');
                        } else {
                            Swal.fire('Oh No!', 'Employee Data failed to save!', 'error');
                            $('#modal-employee').modal('hide');
                        }
                        getEmployeeData();
                    },
                    error: function(err) {
                        $('.saveButton').prop('disabled', false);
                    }
                });
            }
        });
        $('#photo').change(function() {
            readURL(this);
        })
    });

    var employee_table = $('#employee_table').DataTable( {
        responsive: true,
        processing: true,
        ajax: "",
        columns: [
            {
                searchable: false,
                orderable: false,
                data: null,
                defaultContent: ''
            },
            { data: "photo_image" },
            { data: "idcard" },
            { data: "nickname" },
            { data: "fullname" },
            { data: "position" },
            { data: "description" },
            { 
                class: "text-center",
                render: function (data, type, row, meta) {
                    return '<div class="btn-group-vertical"><a href="javascript:void(0);" onclick="editEmployee('+row.id+');" class="btn btn-warning btn-xs">Edit</a><a href="javascript:void(0);" onclick="deleteEmployee('+row.id+');" class="btn btn-danger btn-xs">Delete</a></div>';
                }
            },
        ]
    });

	employee_table.on( 'order.dt search.dt', function () {
        employee_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    function getEmployeeData() {
        employee_table.ajax.url("{{url('employees/getData')}}").load(null, false);
    }

    function resetForm() {
        $('#form-employee').trigger('reset');
        $('#employee_id').val('');
        $('#photo_image').attr('src', "{{asset('assets/images/user.jpg')}}");
        $('#description').summernote('code', '');
    }

    function addEmployee() {
        resetForm();
        $('#modal-employee').modal('show');
    }

    function editEmployee(id) {
        resetForm();
        $.ajax({
            url: "{{url('employees/getEmployee')}}/"+id,
            success: function(data) {
                $('#employee_id').val(data.id);
                $('#photo_image').attr('src', data.photo_image);
                console.log(data.photo_image);
                $('#idcard').val(data.idcard);
                $('#fullname').val(data.fullname);
                $('#nickname').val(data.nickname);
                $('#position').val(data.position);
                $('#description').summernote('code', data.description);
                $('#modal-employee').modal('show');
            },
            error: function(err) {
                Swal.fire('Error!', 'Failed to load data!', 'error');
            }
        })
    }
    
    function addPhoto() {
        $('#photo').trigger('click');
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                filename = $('#photo').val();
                var parts = filename.split('.');
                var ext = parts[parts.length - 1];
                if (!isImage(ext)) {
                    Swal.fire("Warning", "File extension is not allowed!.", "error");
                } else {
                    if (input.files[0].size > 5242880) {
                        Swal.fire("Warning", "Inputed file cannot more than 5MB.", "error");
                    } else {
                        $('#photo_image').attr('src', e.target.result);
                    }
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    function isImage(ext) {
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'jpeg':
            case 'bmp':
            case 'png':
            return true;
        }
        return false;
    }

    function deleteEmployee(id) {
        
        Swal.fire({
            title: 'Are You Sure?',
            text: "Want to delete this Employee data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#FF2C2C',
            confirmButtonText: 'Yes, Delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "{{url('employees/delete')}}",
                    type: "post",
                    data: {_token: '{{csrf_token()}}', id: id},
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire('Great!', 'Employee data deleted successfully!', 'success');
                            getEmployeeData();
                        } else {
                            Swal.fire('Gagal!', 'Employee failed to delete!', 'error');
                        }
                    },
                    error: function(err) {
                        Swal.fire('Error!', 'There is something wrong!', 'error');
                    }
                });
            }
        });
    }
</script>
@endpush