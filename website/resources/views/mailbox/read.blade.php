@extends('layouts.adminlte')
@section('title', 'Inbox')

@section('content')
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title mt-1">{{$message->subject}}</h3>
        <a href="{{url('inbox')}}"><button class="btn btn-sm btn-light float-right">Back</button></a>
    </div>
    <div class="card-body">
        <strong>From : </strong>{{$message->name}} <{{$message->email}}><br>
        <p>{{$message->message}}</p>
    </div>
</div>
@endsection