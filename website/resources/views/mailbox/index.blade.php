@extends('layouts.adminlte')
@section('title', 'Inbox')

@section('content')
<div class="card card-dark">
    <div class="card-header">
        <h3 class="card-title mt-1">Inbox</h3>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="inbox_table">
                <thead>
                    <tr>
                        <th class="text-center" width="10">#</th>
                        <th width="150">From</th>
                        <th>Subject</th>
                        <th width="80">Send Date</th>
                        <th width="10"></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    $(document).ready(function () {
        getInboxData();
    });

    var inbox_table = $('#inbox_table').DataTable( {
        responsive: true,
        processing: true,
        ajax: "",
        columns: [
            {
                searchable: false,
                orderable: false,
                data: null,
                defaultContent: ''
            },
            { data: "from" },
            { data: "subject" },
            { data: "send_date" },
            { 
                class: "text-center",
                render: function (data, type, row, meta) {
                    return '<a href="javascript:void(0);" onclick="deleteMail('+row.id+');" class="btn btn-danger btn-xs">Delete</a>';
                }
            },
        ]
    });

	inbox_table.on( 'order.dt search.dt', function () {
        inbox_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    function getInboxData() {
        inbox_table.ajax.url("{{url('inbox/getData')}}").load(null, false);
    }

    function deleteMail(id) {
        
        Swal.fire({
            title: 'Are You Sure?',
            text: "Want to delete this Mail data!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#FF2C2C',
            confirmButtonText: 'Yes, Delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "{{url('inbox/delete')}}",
                    type: "post",
                    data: {_token: '{{csrf_token()}}', id: id},
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            Swal.fire('Great!', 'Mail deleted successfully!', 'success');
                            getInboxData();
                        } else {
                            Swal.fire('Gagal!', 'Mail failed to delete!', 'error');
                        }
                    },
                    error: function(err) {
                        Swal.fire('Error!', 'There is something wrong!', 'error');
                    }
                });
            }
        });
    }
</script>
@endpush